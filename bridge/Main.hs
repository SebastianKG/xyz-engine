{-# LANGUAGE NamedFieldPuns #-}

import qualified API

main :: IO ()
main = do
  putStrLn "Making Elm bridge files..."
  API.makeClientToServerBridge clientToServerSettings
  API.makeServerToClientBridge serverToClientSettings
  putStrLn "Done making bridge files."

clientToServerSettings :: API.BridgeSettings
clientToServerSettings =
  API.BridgeSettings
    { API.moduleName = "Communication.WebSocket.Compiled.ClientToServer"
    , API.moduleFilepath =
        "./client/Communication/WebSocket/Compiled/ClientToServer.elm"
    }

serverToClientSettings :: API.BridgeSettings
serverToClientSettings =
  API.BridgeSettings
    { API.moduleName = "Communication.WebSocket.Compiled.ServerToClient"
    , API.moduleFilepath =
        "./client/Communication/WebSocket/Compiled/ServerToClient.elm"
    }
