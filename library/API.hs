{-# LANGUAGE NamedFieldPuns #-}

module API
  ( BridgeSettings(..)
  , makeClientToServerBridge
  , makeServerToClientBridge
  ) where

import Data.Proxy (Proxy(..))
import Elm.Module (DefineElm(..), makeModuleContent, moduleHeader)
import Elm.Versions (ElmVersion(..))

import qualified API.WebSocket.ClientToServer as ClientToServer
import qualified API.WebSocket.ServerToClient as ServerToClient

data BridgeSettings = BridgeSettings
  { moduleName :: String
  , moduleFilepath :: FilePath
  }

makeClientToServerBridge :: BridgeSettings -> IO ()
makeClientToServerBridge BridgeSettings {moduleName, moduleFilepath} =
  let elmModule =
        makeElmModule
          moduleName
          [ DefineElm (Proxy :: Proxy ClientToServer.Update)
          , DefineElm (Proxy :: Proxy ClientToServer.UserInput)
          , DefineElm (Proxy :: Proxy ClientToServer.Key)
          ]
   in writeFile moduleFilepath elmModule

makeServerToClientBridge :: BridgeSettings -> IO ()
makeServerToClientBridge BridgeSettings {moduleName, moduleFilepath} =
  let elmModule =
        makeElmModule
          moduleName
          [ DefineElm (Proxy :: Proxy ServerToClient.Update)
          , DefineElm (Proxy :: Proxy ServerToClient.Bounds)
          , DefineElm (Proxy :: Proxy ServerToClient.Physics)
          , DefineElm (Proxy :: Proxy ServerToClient.Kinematics)
          , DefineElm (Proxy :: Proxy ServerToClient.StartingInfo)
          , DefineElm (Proxy :: Proxy ServerToClient.GameStateUpdate)
          , DefineElm (Proxy :: Proxy ServerToClient.Entity)
          , DefineElm (Proxy :: Proxy ServerToClient.Room)
          , DefineElm (Proxy :: Proxy ServerToClient.Number)
          , DefineElm (Proxy :: Proxy ServerToClient.MetricsUpdate)
          , DefineElm (Proxy :: Proxy ServerToClient.MailboxMetrics)
          , DefineElm (Proxy :: Proxy ServerToClient.PortMetrics)
          , DefineElm (Proxy :: Proxy ServerToClient.RobotMetrics)
          , DefineElm (Proxy :: Proxy ServerToClient.GameModel)
          , DefineElm (Proxy :: Proxy ServerToClient.GameBoard)
          , DefineElm (Proxy :: Proxy ServerToClient.Piece)
          , DefineElm (Proxy :: Proxy ServerToClient.PieceColor)
          , DefineElm (Proxy :: Proxy ServerToClient.PieceSize)
          , DefineElm (Proxy :: Proxy ServerToClient.PieceCount)
          , DefineElm (Proxy :: Proxy ServerToClient.DiagonalDirection)
          , DefineElm (Proxy :: Proxy ServerToClient.WinningMethod)
          ]
   in writeFile moduleFilepath elmModule

makeElmModule ::
     String -- ^ Module name
  -> [DefineElm] -- ^ List of definitions to be included in the module
  -> String
makeElmModule moduleName defs =
  unlines
    [ moduleHeader Elm0p18 moduleName
    , ""
    , "import Json.Decode"
    , "import Json.Encode exposing (Value)"
    , "-- The following module comes from bartavelle/json-helpers"
    , "import Json.Helpers exposing (..)"
    , "import Dict"
    , ""
    , ""
    ] ++
  makeModuleContent defs
