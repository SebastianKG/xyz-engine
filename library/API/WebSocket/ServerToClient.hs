{-# LANGUAGE DeriveGeneric, TemplateHaskell #-}

module API.WebSocket.ServerToClient
  ( Physics(..)
  , Bounds(..)
  , Kinematics(..)
  , Update(..)
  , StartingInfo(..)
  , GameStateUpdate(..)
  , Terminated(..)
  , Entity(..)
  , Room(..)
  , MetricsUpdate(..)
  , MailboxMetrics(..)
  , RobotMetrics(..)
  , PortMetrics
  , Number(..)
  , GameModel(..)
  , GameBoard(..)
  , Piece(..)
  , PieceColor(..)
  , PieceSize(..)
  , PieceCount(..)
  , DiagonalDirection(..)
  , WinningMethod(..)
  , representPhysics
  , serialize
  , representMailboxMetrics
  , representRobotMetrics
  , representPortMetrics
  ) where

import Data.Aeson (FromJSON, ToJSON(..))
import qualified Data.Aeson
import qualified Data.Aeson.Types
import qualified Data.Binary.Builder
import Data.ByteString.Lazy (ByteString)
import qualified Data.Double.Conversion.ByteString
import Data.Function ((&))
import Elm.Derive (defaultOptions, deriveBoth, deriveElmDef)
import GHC.Generics (Generic)
import Linear.V2 (V2(..))
import qualified Orvis.Metrics
import qualified Orvis.Unique
import qualified Physics as Domain

newtype Number =
  Number Double
  deriving (Generic, Show)

-- need to be finicky with how JSON conversion works with Doubles, unfortunately
-- reason: performance
instance ToJSON Number where
  toJSON = Data.Aeson.genericToJSON Data.Aeson.defaultOptions
  toEncoding (Number number) =
    number & Data.Double.Conversion.ByteString.toFixed 2 &
    Data.Binary.Builder.fromByteString &
    Data.Aeson.Types.unsafeToEncoding

instance FromJSON Number

deriveElmDef defaultOptions ''Number

data Update
  = BeginGame StartingInfo
  | GameState GameStateUpdate
  | BoardState GameModel
  | Metrics MetricsUpdate

newtype StartingInfo = StartingInfo
  { playerIdentity :: Int
  }

data GameStateUpdate = GameStateUpdate
  { room :: Room
  , entities :: [Entity]
  }

newtype Terminated = Terminated
  { terminatedEntityIdentity :: Int
  }

data Entity = Entity
  { entityIdentity :: Int
  , physics :: Physics
  , metrics :: [(String, Number)]
  , info :: [(String, String)]
  , switches :: [(String, Bool)]
  }

data DiagonalDirection
  = TopLeftBottomRight
  | BottomLeftTopRight

data WinningMethod
  = OtherPlayerLeft
  | DiagonalLine DiagonalDirection
  | HorizontalLine Int
  | VerticalLine Int

data GameModel
  = Loading
  | InProgress GameBoard
  | WonBy Int
          GameBoard
          WinningMethod

data GameBoard = GameBoard
  { blackId :: Int
  , whiteId :: Int
  , blackPieces :: PieceCount
  , whitePieces :: PieceCount
  , board :: [[Maybe Piece]]
  , currentTurn :: PieceColor
  }

data Piece =
  Piece PieceColor
        PieceSize

data PieceColor
  = White
  | Black

data PieceSize
  = Smallest
  | Smaller
  | Bigger
  | Biggest

data PieceCount = PieceCount
  { smallest :: Int
  , smaller :: Int
  , bigger :: Int
  , biggest :: Int
  }

data Room = Room
  { width :: Number
  , height :: Number
  }

newtype Bounds = Circular
  { radius :: Number
  }

data Physics = Physics
  { bounds :: Bounds
  , kinematics :: Kinematics
  , mass :: Number
  }

data MetricsUpdate = MetricsUpdate
  { mailboxes :: [MailboxMetrics]
  , ports :: [PortMetrics]
  , robots :: [RobotMetrics]
  }

data MailboxMetrics = MailboxMetrics
  { mailboxId :: Integer
  , mailboxName :: String
  }

data PortMetrics = PortMetrics
  { portId :: Integer
  , portName :: String
  , subscriptionPoints :: [Integer]
  , broadcastPoints :: [Integer]
  }

data RobotMetrics = RobotMetrics
  { robotId :: Integer
  , robotName :: String
  , readsFrom :: [Integer]
  , writesTo :: [Integer]
  , rendersTo :: [Integer]
  }

representPhysics :: Domain.Physics -> Physics
representPhysics Domain.Physics { Domain.bounds = bounds'
                                , Domain.kinematics = kinematics'
                                , Domain.mass = mass'
                                } =
  let Domain.Circular {Domain.radius = circleRadius} = bounds'
      Domain.Kinematics { Domain.position = V2 x y
                        , Domain.velocity = V2 vx vy
                        , Domain.rotation = rot
                        } = kinematics'
   in Physics
        { bounds = Circular $ Number circleRadius
        , kinematics =
            Kinematics
              { position = (Number x, Number y)
              , velocity = (Number vx, Number vy)
              , rotation = Number rot
              }
        , mass = Number mass'
        }

representMailboxMetrics ::
     Orvis.Unique.Id -> Orvis.Metrics.Mailbox -> MailboxMetrics
representMailboxMetrics identity Orvis.Metrics.Mailbox {Orvis.Metrics.mailboxName = name} =
  MailboxMetrics {mailboxId = Orvis.Unique.raw identity, mailboxName = name}

representRobotMetrics :: Orvis.Unique.Id -> Orvis.Metrics.Robot -> RobotMetrics
representRobotMetrics identity robot =
  let Orvis.Metrics.Robot { Orvis.Metrics.robotName = name
                          , Orvis.Metrics.readsFrom = inboxes
                          , Orvis.Metrics.writesTo = outboxes
                          , Orvis.Metrics.rendersTo = renderboxes
                          } = robot
   in RobotMetrics
        { robotId = Orvis.Unique.raw identity
        , robotName = name
        , readsFrom = Orvis.Unique.raw <$> inboxes
        , writesTo = Orvis.Unique.raw <$> outboxes
        , rendersTo = Orvis.Unique.raw <$> renderboxes
        }

representPortMetrics :: Orvis.Unique.Id -> Orvis.Metrics.Port -> PortMetrics
representPortMetrics identity port =
  let Orvis.Metrics.Port { Orvis.Metrics.portName = name
                         , Orvis.Metrics.subscriptionPoints = inboxes
                         , Orvis.Metrics.broadcastPoints = outboxes
                         } = port
   in PortMetrics
        { portId = Orvis.Unique.raw identity
        , portName = name
        , subscriptionPoints = Orvis.Unique.raw <$> inboxes
        , broadcastPoints = Orvis.Unique.raw <$> outboxes
        }

data Kinematics = Kinematics
  { position :: (Number, Number)
  , velocity :: (Number, Number)
  , rotation :: Number
  }

serialize :: Update -> ByteString
serialize = Data.Aeson.encode

deriveBoth defaultOptions ''Update

deriveBoth defaultOptions ''Physics

deriveBoth defaultOptions ''Bounds

deriveBoth defaultOptions ''Kinematics

deriveBoth defaultOptions ''StartingInfo

deriveBoth defaultOptions ''GameStateUpdate

deriveBoth defaultOptions ''Entity

deriveBoth defaultOptions ''Room

deriveBoth defaultOptions ''MetricsUpdate

deriveBoth defaultOptions ''MailboxMetrics

deriveBoth defaultOptions ''RobotMetrics

deriveBoth defaultOptions ''PortMetrics

deriveBoth defaultOptions ''GameModel

deriveBoth defaultOptions ''WinningMethod

deriveBoth defaultOptions ''DiagonalDirection

deriveBoth defaultOptions ''GameBoard

deriveBoth defaultOptions ''Piece

deriveBoth defaultOptions ''PieceColor

deriveBoth defaultOptions ''PieceSize

deriveBoth defaultOptions ''PieceCount
