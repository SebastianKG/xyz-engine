{-# LANGUAGE TemplateHaskell #-}

module API.WebSocket.ClientToServer
  ( UserInput(..)
  , Update(..)
  , Key(..)
  , deserialize
  ) where

import Data.Aeson (eitherDecode)
import Data.ByteString.Lazy (ByteString)
import Elm.Derive (defaultOptions, deriveBoth)

data Update
  = JoinGame
  | FromUser UserInput
  deriving (Show)

data UserInput
  = Disconnected
  | KeyDown Key
  | KeyUp Key
  | MoveFromTo (Int, Int)
               (Int, Int)
  | MoveNSizedPieceTo Int
                      (Int, Int)
  deriving (Show)

data Key
  = LArrow
  | UArrow
  | RArrow
  | DArrow
  | A
  | B
  | Start
  | Select
  deriving (Show)

deriveBoth defaultOptions ''Update

deriveBoth defaultOptions ''UserInput

deriveBoth defaultOptions ''Key

deserialize :: ByteString -> Either String Update
deserialize = eitherDecode
