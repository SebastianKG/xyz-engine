{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}

module Game
  ( GameFlags(..)
  , Mailboxes(..)
  , Tick(..)
  , PlayerInput(..)
  , PlayerSpawnRequest(..)
  , ExternalMailboxes(..)
  , start
  ) where

import qualified API.WebSocket.ClientToServer as ClientToServer
import Control.Concurrent (threadDelay)
import Control.Monad (forever)
import Control.Monad.IO.Class (liftIO)
import Data.ByteString.Lazy (ByteString)
import qualified Entity.Renderer
import Entity.Renderer (renderer)
import qualified External.Ticker as Ticker
import External.Ticker (TickerFlags(TickerFlags), ticker)
import Orvis.Adapter.Intake (intake, intake_)
import Orvis.Adapter.Outflow (outflow)
import Orvis.Mailbox (Mailbox)
import qualified Orvis.Mailbox as Mailbox
import Orvis.Mailbox (Message(..))
import qualified Orvis.Port as Port
import Orvis.Port (Port(..))
import Orvis.Robot (Robot(..))
import qualified Orvis.Robot as Robot
import Orvis.Topology (Topology)
import Orvis.Unique (Id)

data ExternalMailboxes = ExternalMailboxes
  { externalWebSocketMessagesToSend :: Mailbox ByteString
  , externalPlayerInput :: Mailbox PlayerInput
  , externalNewPlayerRequests :: Mailbox PlayerSpawnRequest
  }

data Mailboxes = Mailboxes
  { clockBox :: Mailbox Tick
  , preRenderer :: Mailbox Entity.Renderer.Input
  , webSocketMessagesToSend :: Mailbox ByteString
  , playerInput :: Mailbox PlayerInput
  , newPlayerRequests :: Mailbox PlayerSpawnRequest
  }

data Tick =
  Tick
  deriving (Show)

data GameFlags = GameFlags
  { externalMailboxes :: ExternalMailboxes
  , gameLogic :: Mailboxes -> Topology ()
  , microsecondsPerTick :: Int
  }

data PlayerInput =
  PlayerInput Id
              ClientToServer.UserInput
  deriving (Show)

newtype PlayerSpawnRequest =
  SpawnPlayerWithId Id
  deriving (Show)

start :: GameFlags -> Topology ()
start flags =
  let GameFlags {externalMailboxes, gameLogic, microsecondsPerTick} = flags
      ExternalMailboxes { externalWebSocketMessagesToSend
                        , externalPlayerInput
                        , externalNewPlayerRequests
                        } = externalMailboxes
   in do clockBox <- Mailbox.spawn "Clockbox"
         preRenderer <- Mailbox.spawn "Pre-renderer"
         -- ticker
         Port.spawn
           "Ticker"
           TickerFlags {Ticker.microsecondsPerTick = microsecondsPerTick}
           Port
             { Port.program = ticker
             , Port.subscriptionPoints = []
             , Port.broadcastPoints =
                 [ clockBox `outflow` \Message {message = Ticker.Tick} ->
                     pure Tick
                 ]
             }
         -- renderer
         Robot.spawn
           "Renderer"
           ()
           Robot
             { Robot.program = renderer
             , Robot.readsFrom =
                 [ intake_ preRenderer
                 , clockBox `intake` \_ Message {message = Tick} ->
                     pure Entity.Renderer.Tick
                 ]
             , Robot.writesTo =
                 [ externalWebSocketMessagesToSend `outflow` \Message {message = Entity.Renderer.Rendered content} ->
                     pure content
                 ]
             , Robot.rendersTo = []
             }
         gameLogic
           Mailboxes
             { clockBox
             , preRenderer
             , webSocketMessagesToSend = externalWebSocketMessagesToSend
             , playerInput = externalPlayerInput
             , newPlayerRequests = externalNewPlayerRequests
             }
         liftIO blockForever

blockForever :: IO ()
blockForever = forever $ threadDelay 1000000
