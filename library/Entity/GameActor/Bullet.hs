{-# LANGUAGE NamedFieldPuns #-}

module Entity.GameActor.Bullet
  ( BulletModel(..)
  , BulletFlags(..)
  , bullet
  ) where

import GameActor (Actor(Actor), Model(..), Output(..), actor)
import qualified GameActor as Actor
import Linear.V2 (V2(..))
import Orvis.Robot.Program (Cmd(..))
import Physics (Bounds(..), Force(..), Kinematics(..), Physics(..))

newtype BulletModel = BulletModel
  { lifeCountdown :: Int
  }

data BulletFlags = BulletFlags
  { lifespan :: Int
  , initialPosition :: V2 Double
  , initialForce :: Force
  , initialRotation :: Double
  , particleMass :: Double
  , particleRadius :: Double
  }

bullet :: Actor.Program BulletFlags BulletModel () () BulletModel
bullet =
  actor
    Actor
      { Actor.initializeActor =
          \flags ->
            let BulletFlags { lifespan
                            , initialPosition
                            , initialForce
                            , initialRotation
                            , particleMass
                            , particleRadius
                            } = flags
             in Actor.Configuration
                  { Actor.initialPhysics =
                      Physics
                        { bounds = Circular {radius = particleRadius}
                        , kinematics =
                            Kinematics
                              { position = initialPosition
                              , velocity = V2 0 0
                              , rotation = initialRotation
                              }
                        , mass = particleMass
                        , frictionCoefficient = 0.1
                        }
                  , Actor.initialForce = initialForce
                  , Actor.initialCustomState =
                      BulletModel {lifeCountdown = lifespan}
                  , Actor.initialCommand = NoOp
                  , Actor.fixedActorType = "bullet"
                  }
      , Actor.tick = tick
      }

tick ::
     BulletFlags
  -> Maybe ()
  -> Model BulletModel
  -> (Model BulletModel, Cmd (Output ()))
tick _ _ model@Model {custom} =
  let BulletModel {lifeCountdown} = custom
   in if lifeCountdown <= 0
        then (model {dead = True}, Die [])
        else ( model {custom = BulletModel {lifeCountdown = lifeCountdown - 1}}
             , NoOp)
