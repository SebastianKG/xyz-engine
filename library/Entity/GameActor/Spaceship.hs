{-# LANGUAGE NamedFieldPuns #-}

module Entity.GameActor.Spaceship
  ( Key(..)
  , KeyChange(..)
  , SpaceshipModel(..)
  , SpaceshipInput(..)
  , SpaceshipOutput(..)
  , SpaceshipFlags(..)
  , spaceship
  ) where

import Control.Monad (guard)
import Data.Maybe (catMaybes)
import Data.Set (Set)
import qualified Data.Set as Set
import GameActor (Actor(Actor), Input, Model(..), Output(..), actor)
import qualified GameActor as Actor
import Linear.Metric (signorm)
import Linear.V2 (V2(..))
import Orvis.Robot.Program (Cmd(..), Program)
import Physics (Bounds(..), Force(..), Kinematics(..), Physics(..))

data SpaceshipModel = SpaceshipModel
  { currentlyHeldKeys :: Set Key
  , thrusting :: Bool
  , cooldown :: Int
  }

newtype SpaceshipInput =
  KeyAction KeyChange

data SpaceshipOutput = ShootBullet
  { bulletPosition :: V2 Double
  , bulletForce :: Force
  }

data KeyChange
  = KeyUp Key
  | KeyDown Key

data Key
  = U
  | D
  | R
  | L
  | Attack
  deriving (Eq, Ord)

data SpaceshipFlags = SpaceshipFlags
  { turningTorqueMagnitude :: Double
  , thrustforceMagnitude :: Double
  , bulletForceMagnitude :: Double
  , cooldownIncrement :: Int
  , bulletOffset :: Double
  , initialPosition :: V2 Double
  , initialRotation :: Double
  , shipRadius :: Double
  , shipMass :: Double
  }

spaceship ::
     Program SpaceshipFlags (Model SpaceshipModel) (Input SpaceshipInput) (Output SpaceshipOutput) (Model SpaceshipModel)
spaceship =
  actor
    Actor
      { Actor.initializeActor =
          \flags ->
            let SpaceshipFlags { initialPosition
                               , initialRotation
                               , shipRadius
                               , shipMass
                               } = flags
             in Actor.Configuration
                  { Actor.initialPhysics =
                      Physics
                        { bounds = Circular {radius = shipRadius}
                        , kinematics =
                            Kinematics
                              { position = initialPosition
                              , velocity = V2 0 0
                              , rotation = initialRotation
                              }
                        , mass = shipMass
                        , frictionCoefficient = 0.3
                        }
                  , Actor.initialForce = Force $ V2 0 0
                  , Actor.initialCustomState =
                      SpaceshipModel
                        { currentlyHeldKeys = Set.empty
                        , thrusting = False
                        , cooldown = 0
                        }
                  , Actor.initialCommand = NoOp
                  , Actor.fixedActorType = "spaceship"
                  }
      , Actor.tick = tick
      }

tick ::
     SpaceshipFlags
  -> Maybe SpaceshipInput
  -> Model SpaceshipModel
  -> (Model SpaceshipModel, Cmd (Output SpaceshipOutput))
tick flags input model@Model { custom = customModel@SpaceshipModel { currentlyHeldKeys
                                                                   , cooldown
                                                                   }
                             , physics = Physics {kinematics = Kinematics { position
                                                                          , rotation
                                                                          }}
                             } =
  let SpaceshipFlags { turningTorqueMagnitude
                     , thrustforceMagnitude
                     , bulletForceMagnitude
                     , cooldownIncrement
                     , bulletOffset
                     } = flags
      pressing :: Key -> Bool
      pressing key = Set.member key currentlyHeldKeys
      turnDirection
        | pressing L && not (pressing R) = Just (-1)
        | pressing R && not (pressing L) = Just 1
        | otherwise = Nothing
      nowThrusting = pressing U
      facingUnitVector =
        signorm $ V2 (cos $ radians rotation) (sin $ radians rotation)
      nowAttacking = pressing Attack
      willShoot = nowAttacking && cooldown <= 1
      newCooldown =
        if willShoot
          then cooldownIncrement
          else max 0 (cooldown - 1)
   in case input of
        Nothing ->
          ( model
              { custom =
                  customModel {thrusting = nowThrusting, cooldown = newCooldown}
              }
          , Batch $
            catMaybes
              [ do guard nowThrusting
                   return . ApplyForce $
                     Force $ fmap (* thrustforceMagnitude) facingUnitVector
              , do direction <- turnDirection
                   return . ApplyAngularVelocity $
                     direction * turningTorqueMagnitude
              , do guard willShoot
                   return . ActorSpecificOutput $
                     ShootBullet
                       { bulletPosition =
                           position + fmap (* bulletOffset) facingUnitVector
                       , bulletForce =
                           Force $
                           fmap (* bulletForceMagnitude) facingUnitVector
                       }
              ])
        Just someInput ->
          case someInput of
            KeyAction keyChange ->
              case keyChange of
                KeyDown key ->
                  ( model
                      { custom =
                          customModel
                            { currentlyHeldKeys =
                                Set.insert key currentlyHeldKeys
                            }
                      }
                  , NoOp)
                KeyUp key ->
                  ( model
                      { custom =
                          customModel
                            { currentlyHeldKeys =
                                Set.delete key currentlyHeldKeys
                            }
                      }
                  , NoOp)

-- degrees to radians
radians :: Double -> Double
radians = (* (pi / 180))
