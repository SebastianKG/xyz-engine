{-# LANGUAGE NamedFieldPuns #-}

module Entity.NestingGame
  ( biggestInCell
  , nestingGame
  , newGame
  , emptyBoard
  , emptyRow
  , Model(..)
  , Input(..)
  , Piece(..)
  , Lobby(..)
  , GameState(..)
  , BoardState(..)
  , Row(..)
  , PlayerPieceCounts(..)
  , Color(..)
  , WinningMethod(..)
  , DiagonalDirection(..)
  , HangingPiece(..)
  ) where

import Control.Monad (guard)
import Data.List (nub, transpose)
import Data.List.Safe ((!!), head, maximum)
import qualified Data.Maybe as Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Linear.V2 (V2(..))
import Orvis.Robot.Program (Cmd(..), Program(..))
import Orvis.Unique (Id)
import Prelude hiding ((!!), head, maximum)

data PlayerPieceCounts = PlayerPieceCounts
  { smallest :: Int
  , smaller :: Int
  , bigger :: Int
  , biggest :: Int
  } deriving (Show, Eq)

data Piece a
  = Smallest a
  | Smaller a
  | Bigger a
  | Biggest a
  deriving (Eq, Show)

instance Functor Piece where
  fmap f piece =
    case piece of
      Smallest a -> Smallest $ f a
      Smaller a -> Smaller $ f a
      Bigger a -> Bigger $ f a
      Biggest a -> Biggest $ f a

instance Eq a => Ord (Piece a) where
  compare p1 p2 = compare (pieceSize p1) (pieceSize p2)

pieceSize :: Piece a -> Int
pieceSize piece =
  case piece of
    Smallest _ -> 0
    Smaller _ -> 1
    Bigger _ -> 2
    Biggest _ -> 3

data Color
  = Black
  | White
  deriving (Eq, Show)

data Row =
  Row [Set (Piece Color)]
  deriving (Show, Eq)

data Model
  = Loading Lobby
  | InProgress GameState
  | WonBy Id
          GameState
          HangingPiece
          WinningMethod
  deriving (Show, Eq)

data WinningMethod
  = OtherPlayerLeft
  | DiagonalLine DiagonalDirection
  | HorizontalLine Int
  | VerticalLine Int
  deriving (Show, Eq)

data DiagonalDirection
  = TopLeftBottomRight
  | BottomLeftTopRight
  deriving (Show, Eq)

data HangingPiece =
  HangingPiece (Maybe (Piece Color))
  deriving (Show, Eq)

data Lobby = Lobby
  { connectedPlayers :: [Id]
  } deriving (Show, Eq)

data GameState = GameState
  { board :: BoardState
  , blackId :: Id
  , whiteId :: Id
  , blackPieces :: PlayerPieceCounts
  , whitePieces :: PlayerPieceCounts
  , currentTurn :: Color
  , lastTurnError :: Maybe String
  } deriving (Show, Eq)

data BoardState =
  BoardState [Row]
  deriving (Show, Eq)

data Input
  = Join Id
  | Leave Id
  | Place (Piece Id)
          (V2 Int)
  | Move Id
         (V2 Int)
         (V2 Int)

nestingGame :: Program () Model Input () Model
nestingGame =
  Program
    { initialize = const (initialModel, NoOp)
    , update = metaUpdate
    , represent = id
    }

initialModel :: Model
initialModel = Loading Lobby {connectedPlayers = []}

metaUpdate :: () -> Input -> Model -> (Model, Cmd ())
metaUpdate _ input model =
  let noCmd m = (m, NoOp)
   in noCmd $
      case model of
        Loading (Lobby playerList) ->
          case input of
            Join identity ->
              let newPlayerList = nub $ identity : playerList
               in case newPlayerList of
                    [p1, p2] -> InProgress $ newGame p1 p2
                    _ -> Loading $ Lobby newPlayerList
            Leave identity -> Loading . Lobby $ filter (/= identity) playerList
            _ -> model
        InProgress gameState -> gameUpdate input gameState
        WonBy _ _ _ _ -> model

gameUpdate :: Input -> GameState -> Model
gameUpdate input gameState =
  let GameState {blackId, whiteId, currentTurn} = gameState
      itsMyTurn :: Id -> Bool
      itsMyTurn identity =
        if identity == blackId
          then currentTurn == Black
          else currentTurn == White
      processResult :: Either String Model -> Model
      processResult result =
        case result of
          Right model ->
            case model of
              Loading _ -> model
              InProgress newGameState ->
                InProgress $ newGameState {lastTurnError = Nothing}
              WonBy _ _ _ _ -> model
          Left err -> InProgress $ gameState {lastTurnError = pure err}
   in processResult $
      case input of
        Join _ -> pure $ InProgress gameState
        Leave identity -> do
          guard (identity == blackId || identity == whiteId)
          pure $
            WonBy
              (if blackId /= identity
                 then blackId
                 else whiteId)
              gameState
              (HangingPiece Nothing)
              OtherPlayerLeft
        Place piece coords -> do
          guard (itsMyTurn $ pieceId piece)
          coloredPiece <- associateWithColor piece gameState
          newPlayerPieces <- takePlayerPiece coloredPiece gameState
          newGameState <-
            updatePlayerPieces newPlayerPieces (pieceColor coloredPiece) <$>
            place coloredPiece coords gameState
          pure $
            case getWinner newGameState of
              Just (winner, winningMethod) ->
                WonBy winner newGameState (HangingPiece Nothing) winningMethod
              _ -> InProgress $ toggleTurn newGameState
        Move identity from to -> do
          guard (itsMyTurn identity)
          (piece, newGameState) <- pickUp identity from gameState
          case getWinner newGameState of
            Just (winner, winningMethod) ->
              pure $
              WonBy
                winner
                newGameState
                (HangingPiece $ pure piece)
                winningMethod
            _ -> do
              lastGameState <- place piece to newGameState
              pure $
                case getWinner lastGameState of
                  Just (winner, winningMethod) ->
                    WonBy
                      winner
                      lastGameState
                      (HangingPiece Nothing)
                      winningMethod
                  _ -> InProgress $ toggleTurn lastGameState

toggleTurn :: GameState -> GameState
toggleTurn gameState@GameState {currentTurn} =
  gameState
    { currentTurn =
        case currentTurn of
          White -> Black
          Black -> White
    }

--
-- initialization
--
newGame :: Id -> Id -> GameState
newGame p1 p2 =
  GameState
    { board = emptyBoard
    , blackId = p1
    , whiteId = p2
    , blackPieces = startingPieces
    , whitePieces = startingPieces
    , currentTurn = White
    , lastTurnError = Nothing
    }

emptyBoard :: BoardState
emptyBoard = BoardState [emptyRow, emptyRow, emptyRow, emptyRow]

emptyRow :: Row
emptyRow = Row [Set.empty, Set.empty, Set.empty, Set.empty]

startingPieces :: PlayerPieceCounts
startingPieces =
  PlayerPieceCounts {smallest = 3, smaller = 3, bigger = 3, biggest = 3}

--
-- player actions
--
pickUp :: Id -> V2 Int -> GameState -> Either String (Piece Color, GameState)
pickUp identity coords gameState =
  let GameState {blackId, whiteId} = gameState
   in do cell <- getCell coords gameState
         color <-
           maybeToEither "Can't pick up from an empty cell" $ cellColor cell
         guard
           (if color == Black
              then identity == blackId
              else identity == whiteId)
         takeBiggest coords gameState

cellColor :: (Set (Piece Color)) -> Maybe Color
cellColor pieceStack = do
  biggestPiece <- biggestInCell pieceStack
  pure $ pieceColor biggestPiece

pieceColor :: Piece Color -> Color
pieceColor = pieceAttribute

pieceId :: Piece Id -> Id
pieceId = pieceAttribute

pieceAttribute :: Piece a -> a
pieceAttribute piece =
  case piece of
    Smallest a -> a
    Smaller a -> a
    Bigger a -> a
    Biggest a -> a

takeBiggest :: V2 Int -> GameState -> Either String (Piece Color, GameState)
takeBiggest coords gameState =
  let transformCell ::
           Piece Color -> Set (Piece Color) -> Either String (Set (Piece Color))
      transformCell biggest = pure . Set.filter (/= biggest)
   in do cell <- getCell coords gameState
         biggestPiece <-
           maybeToEither "Can't take biggest from an empty cell" $
           biggestInCell cell
         newCell <- transformCell biggestPiece cell
         pure $ (biggestPiece, updateCell coords newCell gameState)

getCell :: V2 Int -> GameState -> Either String (Set (Piece Color))
getCell (V2 x y) gameState =
  let GameState {board} = gameState
      BoardState boardState = board
      errorMsg =
        concat
          [ "Invalid coordinates ("
          , show x
          , ", "
          , show y
          , ") for retrieving cell."
          ]
   in maybeToEither errorMsg $ do
        row <- boardState !! y
        let Row rowState = row
        rowState !! x

updateCell :: V2 Int -> Set (Piece Color) -> GameState -> GameState
updateCell (V2 x y) pieces gameState =
  let GameState {board} = gameState
      BoardState boardState = board
   in gameState
        { board =
            BoardState
              (zipWith
                 (\yIndex row ->
                    let Row rowState = row
                     in Row $
                        zipWith
                          (\xIndex cell ->
                             if xIndex == x && yIndex == y
                               then pieces
                               else cell)
                          [0 ..]
                          rowState)
                 [0 ..]
                 boardState)
        }

place :: Piece Color -> V2 Int -> GameState -> Either String GameState
place piece coords gameState = do
  cell <- getCell coords gameState
  let newPieceIsBigger =
        case biggestInCell cell of
          Nothing -> True
          Just previousBiggest -> piece > previousBiggest
  guard (newPieceIsBigger)
  let newCell = Set.insert piece cell
  pure $ updateCell coords newCell gameState

takePlayerPiece :: Piece Color -> GameState -> Either String PlayerPieceCounts
takePlayerPiece piece GameState {blackPieces, whitePieces} =
  let err specificMsg =
        Left $
        concat
          [ specificMsg
          , ": piece was "
          , show (pieceColor piece)
          , " and of size "
          , show (pieceSize piece)
          ]
      noneLeft = "No more pieces of this size left"
      takePiece pieces@PlayerPieceCounts {smallest, smaller, bigger, biggest} =
        case pieceSize piece of
          0 ->
            if smallest <= 0
              then err noneLeft
              else pure $ pieces {smallest = smallest - 1}
          1 ->
            if smaller <= 0
              then err noneLeft
              else pure $ pieces {smaller = smaller - 1}
          2 ->
            if bigger <= 0
              then err noneLeft
              else pure $ pieces {bigger = bigger - 1}
          3 ->
            if biggest <= 0
              then err noneLeft
              else pure $ pieces {biggest = biggest - 1}
          _ -> err "Invalid piece size to use"
   in case pieceColor piece of
        White -> takePiece whitePieces
        Black -> takePiece blackPieces

updatePlayerPieces :: PlayerPieceCounts -> Color -> GameState -> GameState
updatePlayerPieces pieces color gameState =
  case color of
    White -> gameState {whitePieces = pieces}
    Black -> gameState {blackPieces = pieces}

associateWithColor :: Piece Id -> GameState -> Either String (Piece Color)
associateWithColor piece gameState
  | myId == whiteId = pure $ const White <$> piece
  | myId == blackId = pure $ const Black <$> piece
  | otherwise = Left "Identity could not be associated to a color"
  where
    myId = pieceId piece
    GameState {whiteId, blackId} = gameState

biggestInCell :: Eq a => Set (Piece a) -> Maybe (Piece a)
biggestInCell = maximum . Set.toList

-- 
-- victory inspection
-- 
getWinner :: GameState -> Maybe (Id, WinningMethod)
getWinner gameState =
  let GameState {board, whiteId, blackId} = gameState
      BoardState boardState = board
      win color method =
        ( case color of
            White -> whiteId
            Black -> blackId
        , method)
      reversedBoardState = reverseRows boardState
      transposedBoardState = transposeRows boardState
      diagonalWin diagonalType givenBoard = do
        Row first <- givenBoard !! (0 :: Int)
        firstCell <- first !! (0 :: Int)
        Row second <- givenBoard !! (1 :: Int)
        secondCell <- second !! (1 :: Int)
        Row third <- givenBoard !! (2 :: Int)
        thirdCell <- third !! (2 :: Int)
        Row fourth <- givenBoard !! (3 :: Int)
        fourthCell <- fourth !! (3 :: Int)
        color <- wonRow $ Row [firstCell, secondCell, thirdCell, fourthCell]
        pure $ win color (DiagonalLine diagonalType)
   in head $
      Maybe.catMaybes $
      concat
        [ zipWith
            (\i row -> do
               color <- wonRow row
               pure $ win color (HorizontalLine i))
            [0 ..]
            boardState
        , zipWith
            (\i row -> do
               color <- wonRow row
               pure $ win color (VerticalLine i))
            [0 ..] $
          transposedBoardState
        , singleton $ diagonalWin TopLeftBottomRight boardState
        , singleton $ diagonalWin BottomLeftTopRight reversedBoardState
        ]

wonRow :: Row -> Maybe Color
wonRow (Row rowState)
  | all (== pure White) $ fmap cellColor rowState = pure White
  | all (== pure Black) $ fmap cellColor rowState = pure Black
  | otherwise = Nothing

reverseRows :: [Row] -> [Row]
reverseRows = fmap (\(Row list) -> Row $ reverse list)

transposeRows :: [Row] -> [Row]
transposeRows =
  fmap (\list -> Row list) . transpose . fmap (\(Row list) -> list)

singleton :: a -> [a]
singleton a = [a]

maybeToEither :: err -> Maybe a -> Either err a
maybeToEither err value =
  case value of
    Just v -> Right v
    Nothing -> Left err
