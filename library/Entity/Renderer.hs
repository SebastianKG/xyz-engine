{-# LANGUAGE NamedFieldPuns #-}

module Entity.Renderer
  ( renderer
  , Input(..)
  , Output(..)
  ) where

import qualified API.WebSocket.ServerToClient as Representation
import Data.ByteString.Lazy (ByteString)
import qualified Data.Map as Map
import Data.Map.Strict (Map)
import Orvis.Robot.Program (Cmd(..), Program(..))

data Input
  = Tick
  | Entity Representation.Entity
  | EntityTermination Representation.Terminated
  | BoardGame Representation.GameModel
  | Room Representation.Room

newtype Output =
  Rendered ByteString

data GameState = GameState
  { room :: Maybe Representation.Room
  , entities :: Map Int Representation.Entity
  , terminatedEntityIds :: [Int]
  }

newtype Model = Model
  { currentState :: GameState
  }

startingState :: GameState
startingState =
  GameState {room = Nothing, entities = Map.empty, terminatedEntityIds = []}

renderer :: Program () Model Input Output ()
renderer =
  Program
    { initialize = const (Model {currentState = startingState}, NoOp)
    , update = update'
    , represent = const ()
    }

update' :: () -> Input -> Model -> (Model, Cmd Output)
update' _ input model@Model {currentState} =
  let GameState {entities, terminatedEntityIds} = currentState
   in case input of
        Tick ->
          case toRepresentation currentState of
            Just state ->
              ( model
                  { currentState =
                      currentState
                        { entities =
                            Map.filterWithKey
                              (\identity _ ->
                                 identity `notElem` terminatedEntityIds)
                              entities
                        , terminatedEntityIds = []
                        }
                  }
              , Cmd . Rendered . Representation.serialize $
                Representation.GameState state)
            Nothing -> (model, NoOp)
        Entity entity@Representation.Entity {Representation.entityIdentity = myId} ->
          ( model
              { currentState =
                  currentState {entities = Map.insert myId entity entities}
              }
          , NoOp)
        EntityTermination termination ->
          ( model
              { currentState =
                  currentState
                    { terminatedEntityIds =
                        Representation.terminatedEntityIdentity termination :
                        terminatedEntityIds
                    }
              }
          , NoOp)
        BoardGame gameModel ->
          ( model
          , Cmd . Rendered . Representation.serialize $
            Representation.BoardState gameModel)
        Room room ->
          (model {currentState = currentState {room = Just room}}, NoOp)

toRepresentation :: GameState -> Maybe Representation.GameStateUpdate
toRepresentation GameState {room, entities} = do
  r <- room
  return
    Representation.GameStateUpdate
      {Representation.room = r, Representation.entities = Map.elems entities}
