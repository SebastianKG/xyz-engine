{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

-- The Doubles used in this package rely on the default config of Chipmunk2D, 
-- where cpFloat is defined as a Double and not a Float
module Language.C.Inline.Context.Chipmunk2d
  ( ctx
  , Body
  , Space
  , Shape
  , Constraint
  , Vect(..)
  , Kinematics(..)
  ) where

import qualified Data.Map as Map
import Data.Monoid ((<>))
import Foreign.Storable (Storable(..))
import Foreign.Storable.Record as Store
import qualified Language.C.Inline.Context as C
import qualified Language.C.Types as C

data Body

data Shape

data Space

data Constraint

data Vect = Vect
  { x :: Double
  , y :: Double
  }

data Kinematics = Kinematics
  { position :: Vect
  , velocity :: Vect
  , rotation :: Double
  }

storeVect :: Store.Dictionary Vect
storeVect = Store.run $ Vect <$> Store.element x <*> Store.element y

storeKinematics :: Store.Dictionary Kinematics
storeKinematics =
  Store.run $
  Kinematics <$> Store.element position <*> Store.element velocity <*>
  Store.element rotation

instance Storable Vect where
  sizeOf = Store.sizeOf storeVect
  alignment = Store.alignment storeVect
  peek = Store.peek storeVect
  poke = Store.poke storeVect

instance Storable Kinematics where
  sizeOf = Store.sizeOf storeKinematics
  alignment = Store.alignment storeKinematics
  peek = Store.peek storeKinematics
  poke = Store.poke storeKinematics

ctx :: C.Context
ctx = C.baseCtx <> custom
  where
    custom =
      mempty
        { C.ctxTypesTable =
            Map.fromList
              [ (C.TypeName "cpBody", [t|Body|])
              , (C.TypeName "cpShape", [t|Shape|])
              , (C.TypeName "cpSpace", [t|Space|])
              , (C.TypeName "cpConstraint", [t|Constraint|])
              , (C.TypeName "cpVect", [t|Vect|])
              , (C.TypeName "xyzKinematics", [t|Kinematics|])
              ]
        }
