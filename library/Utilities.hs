{-# LANGUAGE NamedFieldPuns #-}

module Utilities
  ( watchAndRecord
  , watchAndRecordMapped
  ) where

import Control.Concurrent (forkIO)
import Control.Monad (forever, void)
import Orvis.Mailbox (Mailbox, Message(..))
import qualified Orvis.Mailbox as Mailbox

watchAndRecord :: Show a => String -> Mailbox a -> IO ()
watchAndRecord = watchAndRecordMapped (pure . show)

watchAndRecordMapped :: (a -> Maybe String) -> String -> Mailbox a -> IO ()
watchAndRecordMapped f name mailbox = do
  outbox <- Mailbox.subscribe mailbox
  void . forkIO . void . forever $ do
    Message {message} <- Mailbox.next outbox
    case f message of
      Just msg ->
        appendFile (name ++ ".log") $ "Received a message " ++ msg ++ "\n"
      Nothing -> return ()
