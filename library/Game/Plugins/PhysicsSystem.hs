{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}

module Game.Plugins.PhysicsSystem
  ( Mailboxes(..)
  , EntityLifecycleEvent(..)
  , connect
  ) where

import qualified API.WebSocket.ServerToClient as ServerToClient
import qualified Entity.Renderer
import External.Simulator (SimulatorFlags, simulator)
import qualified External.Simulator as Simulator
import qualified Game
import Orvis.Adapter.Intake (intake)
import Orvis.Adapter.Outflow (outflow)
import qualified Orvis.Mailbox as Mailbox
import Orvis.Mailbox (Mailbox)
import Orvis.Mailbox (Message(..))
import Orvis.Port (Port(Port))
import qualified Orvis.Port as Port
import Orvis.Topology (Topology)
import qualified Physics as Physics
import Physics (Physics)

data Mailboxes = Mailboxes
  { forcesToApply :: Mailbox Physics.Force
  , angularVelocitiesToApply :: Mailbox Double
  , entityLifecycleEvents :: Mailbox EntityLifecycleEvent
  , evaluatedWorlds :: Mailbox Physics.EvaluatedWorld
  }

data EntityLifecycleEvent
  = Initialized Physics
                Physics.Force
  | Dead

connect :: Game.Mailboxes -> SimulatorFlags -> Topology Mailboxes
connect basicMailboxes simulatorFlags =
  let Game.Mailboxes {clockBox, preRenderer} = basicMailboxes
   in do forcesToApply <- Mailbox.spawn "Forces to Apply"
         angularVelocitiesToApply <- Mailbox.spawn "Angular Velocities to Apply"
         entityLifecycleEvents <- Mailbox.spawn "Entity Lifecycle Events"
         evaluatedWorlds <- Mailbox.spawn "Evaluated Worlds"
         -- simulator
         Port.spawn
           "Simulator"
           simulatorFlags
           Port
             { Port.program = simulator
             , Port.subscriptionPoints =
                 [ clockBox `intake` \_ _ -> pure Simulator.Tick
                 , forcesToApply `intake` \_ Message { message = force
                                                     , identity = senderId
                                                     } ->
                     pure $
                     Simulator.EntityAction senderId $
                     Simulator.ApplyForce force
                 , angularVelocitiesToApply `intake` \_ Message { message = av
                                                                , identity = senderId
                                                                } ->
                     pure $
                     Simulator.EntityAction senderId $
                     Simulator.ApplyAngularVelocity av
                 , entityLifecycleEvents `intake` \_ Message { message = msg
                                                             , identity = senderId
                                                             } ->
                     case msg of
                       Initialized initialPhysics initialForce ->
                         pure $
                         Simulator.EntityAction senderId $
                         Simulator.Initialize initialPhysics initialForce
                       Dead ->
                         pure $ Simulator.EntityAction senderId Simulator.Remove
                 ]
             , Port.broadcastPoints =
                 [ evaluatedWorlds `outflow` \Message {message = msg} ->
                     case msg of
                       Simulator.SimulationResult evaluatedWorld ->
                         pure evaluatedWorld
                       _ -> Nothing
                 , preRenderer `outflow` \Message {message = msg} ->
                     case msg of
                       Simulator.CurrentRoom width height ->
                         pure $
                         Entity.Renderer.Room $
                         ServerToClient.Room
                           (ServerToClient.Number width)
                           (ServerToClient.Number height)
                       _ -> Nothing
                 ]
             }
         pure Mailboxes {..}
