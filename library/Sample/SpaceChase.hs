{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Sample.SpaceChase
  ( logic
  ) where

import qualified API.WebSocket.ClientToServer as ClientToServer
import qualified API.WebSocket.ServerToClient as ServerToClient
import Control.Arrow (second)
import qualified Data.Map as Map
import Entity.GameActor.Bullet (BulletFlags(BulletFlags), BulletModel, bullet)
import qualified Entity.GameActor.Bullet as Bullet
import Entity.GameActor.Spaceship
  ( SpaceshipFlags(SpaceshipFlags)
  , SpaceshipInput
  , SpaceshipModel
  , SpaceshipOutput
  , spaceship
  )
import qualified Entity.GameActor.Spaceship as Spaceship
import qualified Entity.Renderer
import qualified External.Simulator as Simulator
import External.Simulator (SimulatorFlags(SimulatorFlags))
import qualified Game
import Game (PlayerInput(..), PlayerSpawnRequest(..))
import qualified Game.Plugins.PhysicsSystem
import Game.Plugins.PhysicsSystem (EntityLifecycleEvent(..))
import qualified GameActor
import Linear.V2 (V2(..))
import Orvis.Adapter.Intake (Intake, intake)
import Orvis.Adapter.Outflow (Outflow, outflow)
import Orvis.Mailbox (Mailbox, Message(..))
import qualified Orvis.Mailbox as Mailbox
import Orvis.Port (Port(Port))
import qualified Orvis.Port as Port
import Orvis.Port.Program.Spawner (spawner)
import qualified Orvis.Port.Program.Spawner as Spawner
import Orvis.Robot (Robot(Robot))
import qualified Orvis.Robot as Robot
import Orvis.Topology (Topology)
import Orvis.Unique (Id)
import qualified Orvis.Unique as Unique
import qualified Physics

logic :: Game.Mailboxes -> Topology ()
logic basicMailboxes = do
  physicsSystemMailboxes <-
    Game.Plugins.PhysicsSystem.connect
      basicMailboxes
      SimulatorFlags {Simulator.roomWidth = 640, Simulator.roomHeight = 480}
  let Game.Mailboxes {preRenderer, playerInput, newPlayerRequests} =
        basicMailboxes
      Game.Plugins.PhysicsSystem.Mailboxes { forcesToApply
                                           , angularVelocitiesToApply
                                           , entityLifecycleEvents
                                           , evaluatedWorlds
                                           } = physicsSystemMailboxes
  newBulletRequests :: Mailbox BulletSpawnRequest <-
    Mailbox.spawn "New Bullet Requests"
  Port.spawn
    "Player Spawner"
    ()
    Port
      { Port.program =
          spawner $
          mapPlayerRequestToProgram
            DataFlows
              { readIntakes =
                  [ playerInput `intake` translateKeyToSpaceshipInput
                  , evaluatedWorlds `intake` \myId Message {message = Physics.EvaluatedWorld evaluated} ->
                      pure $
                      case Map.lookup myId evaluated of
                        Just myNewKinematics ->
                          GameActor.PhysicsUpdate myNewKinematics
                        Nothing -> GameActor.NoPhysicsUpdateTick
                  ]
              , outFlows =
                  [ forcesToApply `outflow` \Message {message = msg} ->
                      case msg of
                        GameActor.ApplyForce f -> pure f
                        _ -> Nothing
                  , angularVelocitiesToApply `outflow` \Message {message = msg} ->
                      case msg of
                        GameActor.ApplyAngularVelocity av -> pure av
                        _ -> Nothing
                  , entityLifecycleEvents `outflow` \Message {message = msg} ->
                      case msg of
                        GameActor.Initialized physics f ->
                          pure $ Initialized physics f
                        GameActor.Dead -> pure Dead
                        _ -> Nothing
                  , newBulletRequests `outflow` \Message {message = msg} ->
                      case msg of
                        GameActor.ActorSpecificOutput Spaceship.ShootBullet { Spaceship.bulletPosition = p
                                                                            , Spaceship.bulletForce = f
                                                                            } ->
                          pure
                            BulletSpawnRequest
                              {initialPosition = p, initialForce = f}
                        _ -> Nothing
                  ]
              , renderFlows =
                  [ preRenderer `outflow`
                    (pure . representGameActor (const emptyFacts))
                  ]
              }
      , Port.subscriptionPoints =
          [ newPlayerRequests `intake` \_ Message {message = SpawnPlayerWithId playerId} ->
              pure $ Spawner.SpawnWithId playerId (SpawnPlayerWithId playerId)
          ]
      , Port.broadcastPoints = []
      }
  Port.spawn
    "Bullet Spawner"
    ()
    Port
      { Port.program =
          spawner $
          mapBulletRequestToProgram
            DataFlows
              { readIntakes =
                  [ evaluatedWorlds `intake` \myId Message {message = Physics.EvaluatedWorld evaluated} ->
                      pure $
                      case Map.lookup myId evaluated of
                        Just myNewKinematics ->
                          GameActor.PhysicsUpdate myNewKinematics
                        Nothing -> GameActor.NoPhysicsUpdateTick
                  ]
              , outFlows =
                  [ forcesToApply `outflow` \Message {message = msg} ->
                      case msg of
                        GameActor.ApplyForce f -> pure f
                        _ -> Nothing
                  , entityLifecycleEvents `outflow` \Message {message = msg} ->
                      case msg of
                        GameActor.Initialized physics f ->
                          pure $ Initialized physics f
                        GameActor.Dead -> pure Dead
                        _ -> Nothing
                  ]
              , renderFlows =
                  [ preRenderer `outflow`
                    (pure . representGameActor (const emptyFacts))
                  ]
              }
      , Port.subscriptionPoints =
          [ newBulletRequests `intake` \_ Message {message = msg@BulletSpawnRequest {}} ->
              pure $ Spawner.Spawn msg
          ]
      , Port.broadcastPoints = []
      }

data BulletSpawnRequest = BulletSpawnRequest
  { initialPosition :: V2 Double
  , initialForce :: Physics.Force
  } deriving (Show)

data RepresentationFacts = RepresentationFacts
  { metrics :: [(String, Double)]
  , info :: [(String, String)]
  , switches :: [(String, Bool)]
  }

emptyFacts :: RepresentationFacts
emptyFacts = RepresentationFacts {metrics = [], info = [], switches = []}

-- Adapter Functions
representGameActor ::
     (custom -> RepresentationFacts)
  -> Message (GameActor.Model custom)
  -> Entity.Renderer.Input
representGameActor actorFacts Message { identity = playerId
                                      , message = GameActor.Model { GameActor.physics = physics
                                                                  , GameActor.dead = dead
                                                                  , GameActor.actorType = actorType
                                                                  , GameActor.custom = custom
                                                                  }
                                      } =
  let RepresentationFacts {metrics, info, switches} = actorFacts custom
   in if dead
        then Entity.Renderer.EntityTermination
               ServerToClient.Terminated
                 { ServerToClient.terminatedEntityIdentity =
                     fromIntegral $ Unique.raw playerId
                 }
        else Entity.Renderer.Entity
               ServerToClient.Entity
                 { ServerToClient.entityIdentity =
                     fromIntegral $ Unique.raw playerId
                 , ServerToClient.physics =
                     ServerToClient.representPhysics physics
                 , ServerToClient.metrics =
                     second ServerToClient.Number <$> metrics
                 , ServerToClient.info = ("entityType", actorType) : info
                 , ServerToClient.switches = switches
                 }

convertKey :: ClientToServer.Key -> Maybe Spaceship.Key
convertKey key =
  case key of
    ClientToServer.LArrow -> pure Spaceship.L
    ClientToServer.DArrow -> pure Spaceship.D
    ClientToServer.RArrow -> pure Spaceship.R
    ClientToServer.UArrow -> pure Spaceship.U
    ClientToServer.A -> pure Spaceship.Attack
    ClientToServer.B -> pure Spaceship.Attack
    ClientToServer.Start -> Nothing
    ClientToServer.Select -> Nothing

translateKeyToSpaceshipInput ::
     Id -> Message PlayerInput -> Maybe (GameActor.Input SpaceshipInput)
translateKeyToSpaceshipInput myId Message {message = PlayerInput playerId msg}
  | myId == playerId =
    case msg of
      ClientToServer.Disconnected -> pure GameActor.SelfDestruct
      ClientToServer.KeyDown key -> do
        converted <- convertKey key
        return $
          GameActor.ActorSpecificInput $
          Spaceship.KeyAction $ Spaceship.KeyDown converted
      ClientToServer.KeyUp key -> do
        converted <- convertKey key
        return $
          GameActor.ActorSpecificInput $
          Spaceship.KeyAction $ Spaceship.KeyUp converted
      _ -> Nothing
  | otherwise = Nothing

data DataFlows input output representation = DataFlows
  { readIntakes :: [Intake input]
  , outFlows :: [Outflow output]
  , renderFlows :: [Outflow representation]
  }

mapPlayerRequestToProgram ::
     DataFlows (GameActor.Input SpaceshipInput) (GameActor.Output SpaceshipOutput) (GameActor.Model SpaceshipModel)
  -> PlayerSpawnRequest
  -> ( String
     , SpaceshipFlags
     , GameActor.Robot SpaceshipFlags SpaceshipModel SpaceshipInput SpaceshipOutput SpaceshipModel)
mapPlayerRequestToProgram DataFlows {readIntakes, outFlows, renderFlows} msg =
  case msg of
    SpawnPlayerWithId _ ->
      ( "Spaceship"
      , SpaceshipFlags
          { Spaceship.turningTorqueMagnitude = -300
          , Spaceship.thrustforceMagnitude = 6000
          , Spaceship.bulletForceMagnitude = 650
          , Spaceship.cooldownIncrement = 4
          , Spaceship.bulletOffset = 18
          , Spaceship.initialPosition = V2 0 0
          , Spaceship.initialRotation = 0
          , Spaceship.shipRadius = 15
          , Spaceship.shipMass = 5
          }
      , Robot
          { Robot.program = spaceship
          , Robot.readsFrom = readIntakes
          , Robot.writesTo = outFlows
          , Robot.rendersTo = renderFlows
          })

mapBulletRequestToProgram ::
     DataFlows (GameActor.Input ()) (GameActor.Output ()) (GameActor.Model BulletModel)
  -> BulletSpawnRequest
  -> ( String
     , BulletFlags
     , GameActor.Robot BulletFlags BulletModel () () BulletModel)
mapBulletRequestToProgram DataFlows {readIntakes, outFlows, renderFlows} msg =
  case msg of
    BulletSpawnRequest {initialPosition, initialForce} ->
      ( "Bullet"
      , BulletFlags
          { Bullet.lifespan = 160
          , Bullet.initialPosition = initialPosition
          , Bullet.initialForce = initialForce
          , Bullet.initialRotation = 0
          , Bullet.particleMass = 5
          , Bullet.particleRadius = 6
          }
      , Robot
          { Robot.program = bullet
          , Robot.readsFrom = readIntakes
          , Robot.writesTo = outFlows
          , Robot.rendersTo = renderFlows
          })
