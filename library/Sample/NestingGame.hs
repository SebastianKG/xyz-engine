{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}

module Sample.NestingGame
  ( logic
  ) where

import API.WebSocket.ClientToServer (UserInput(..))
import qualified API.WebSocket.ServerToClient as ServerToClient
import Entity.NestingGame (nestingGame)
import qualified Entity.NestingGame as NestingGame
import qualified Entity.Renderer as Renderer
import qualified Game
import Game (Mailboxes(..))
import Linear.V2 (V2(..))
import Orvis.Adapter.Intake (intake)
import Orvis.Adapter.Outflow (outflow)
import Orvis.Mailbox (Message(..))
import qualified Orvis.Robot as Robot
import Orvis.Robot (Robot(Robot))
import Orvis.Topology (Topology)
import qualified Orvis.Unique as Unique

logic :: Game.Mailboxes -> Topology ()
logic basicMailboxes =
  let Game.Mailboxes {preRenderer, playerInput, newPlayerRequests} =
        basicMailboxes
   in Robot.spawn
        "Nesting Game"
        ()
        Robot
          { Robot.program = nestingGame
          , Robot.readsFrom =
              [ newPlayerRequests `intake` \_ Message {message = Game.SpawnPlayerWithId playerId} ->
                  pure $ NestingGame.Join playerId
              , playerInput `intake` \_ Message {message = Game.PlayerInput playerId msg} ->
                  case msg of
                    Disconnected -> pure $ NestingGame.Leave playerId
                    MoveFromTo (fromX, fromY) (toX, toY) ->
                      pure $
                      NestingGame.Move playerId (V2 fromX fromY) (V2 toX toY)
                    MoveNSizedPieceTo pieceSize (x, y) -> do
                      piece <-
                        case pieceSize of
                          0 -> pure $ NestingGame.Smallest playerId
                          1 -> pure $ NestingGame.Smaller playerId
                          2 -> pure $ NestingGame.Bigger playerId
                          3 -> pure $ NestingGame.Biggest playerId
                          _ -> Nothing
                      pure $ NestingGame.Place piece (V2 x y)
                    _ -> Nothing
              ]
          , Robot.writesTo = []
          , Robot.rendersTo =
              [ preRenderer `outflow` \Message {message = model} ->
                  pure $ Renderer.BoardGame $ representNestingGame model
              ]
          }

representNestingGame :: NestingGame.Model -> ServerToClient.GameModel
representNestingGame model =
  case model of
    NestingGame.Loading _ -> ServerToClient.Loading
    NestingGame.InProgress gameState ->
      ServerToClient.InProgress $ representGameState gameState
    NestingGame.WonBy winnerId gameState _ winningMethod ->
      ServerToClient.WonBy
        (fromIntegral $ Unique.raw winnerId)
        (representGameState gameState)
        (case winningMethod of
           NestingGame.OtherPlayerLeft -> ServerToClient.OtherPlayerLeft
           NestingGame.DiagonalLine diagonalDirection ->
             case diagonalDirection of
               NestingGame.TopLeftBottomRight ->
                 ServerToClient.DiagonalLine ServerToClient.TopLeftBottomRight
               NestingGame.BottomLeftTopRight ->
                 ServerToClient.DiagonalLine ServerToClient.BottomLeftTopRight
           NestingGame.HorizontalLine i -> ServerToClient.HorizontalLine i
           NestingGame.VerticalLine i -> ServerToClient.VerticalLine i)

representGameState :: NestingGame.GameState -> ServerToClient.GameBoard
representGameState gameState =
  let NestingGame.GameState { board
                            , blackId
                            , whiteId
                            , blackPieces
                            , whitePieces
                            , currentTurn
                            } = gameState
   in ServerToClient.GameBoard
        { ServerToClient.blackId = fromIntegral $ Unique.raw blackId
        , ServerToClient.whiteId = fromIntegral $ Unique.raw whiteId
        , ServerToClient.blackPieces = representPlayerPieces blackPieces
        , ServerToClient.whitePieces = representPlayerPieces whitePieces
        , ServerToClient.board = representBoardState board
        , ServerToClient.currentTurn = representColor currentTurn
        }

representPlayerPieces ::
     NestingGame.PlayerPieceCounts -> ServerToClient.PieceCount
representPlayerPieces pieces =
  let NestingGame.PlayerPieceCounts {smallest, smaller, bigger, biggest} =
        pieces
   in ServerToClient.PieceCount
        { ServerToClient.smallest = smallest
        , ServerToClient.smaller = smaller
        , ServerToClient.bigger = bigger
        , ServerToClient.biggest = biggest
        }

representBoardState :: NestingGame.BoardState -> [[Maybe ServerToClient.Piece]]
representBoardState boardState =
  let NestingGame.BoardState rows = boardState
      viewRow (NestingGame.Row row) = viewCell <$> row
      viewCell pieceSet = do
        topPiece <- NestingGame.biggestInCell pieceSet
        pure $
          case topPiece of
            NestingGame.Smallest color ->
              ServerToClient.Piece
                (representColor color)
                ServerToClient.Smallest
            NestingGame.Smaller color ->
              ServerToClient.Piece (representColor color) ServerToClient.Smaller
            NestingGame.Bigger color ->
              ServerToClient.Piece (representColor color) ServerToClient.Bigger
            NestingGame.Biggest color ->
              ServerToClient.Piece (representColor color) ServerToClient.Biggest
   in viewRow <$> rows

representColor :: NestingGame.Color -> ServerToClient.PieceColor
representColor color =
  case color of
    NestingGame.White -> ServerToClient.White
    NestingGame.Black -> ServerToClient.Black
