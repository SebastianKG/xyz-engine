{-# LANGUAGE NamedFieldPuns #-}

module GameActor
  ( Actor(..)
  , Configuration(..)
  , Input(..)
  , Output(..)
  , Model(..)
  , Program
  , Robot
  , actor
  , updateKinematics
  ) where

import Data.Monoid ((<>))
import qualified Orvis.Robot as Orvis
import Orvis.Robot.Program (Cmd(..))
import qualified Orvis.Robot.Program as Orvis
import Physics (Force, Kinematics, Physics(..))

data Input customInput
  = SelfDestruct
  | PhysicsUpdate Kinematics
  | NoPhysicsUpdateTick
  | ActorSpecificInput customInput

data Output customOutput
  = Initialized Physics
                Force
  | ApplyForce Force
  | ApplyAngularVelocity Double
  | ActorSpecificOutput customOutput
  | Dead

data Model custom = Model
  { dead :: Bool
  , physics :: Physics
  , actorType :: String
  , custom :: custom
  }

updateKinematics :: Kinematics -> Model custom -> Model custom
updateKinematics newKinematics model@Model {physics} =
  model {physics = physics {kinematics = newKinematics}}

data Configuration flags customModel output = Configuration
  { initialPhysics :: Physics
  , initialForce :: Force
  , initialCustomState :: customModel
  , initialCommand :: Cmd (Output output)
  , fixedActorType :: String
  }

data Actor flags customModel customInput customOutput = Actor
  { initializeActor :: flags -> Configuration flags customModel customOutput
  , tick :: flags -> Maybe customInput -> Model customModel -> ( Model customModel
                                                               , Cmd (Output customOutput))
  }

type Program c m i o r
   = Orvis.Program c (Model m) (Input i) (Output o) (Model r)

type Robot c m i o r = Orvis.Robot c (Model m) (Input i) (Output o) (Model r)

actor ::
     Actor flags customModel customInput customOutput
  -> Program flags customModel customInput customOutput customModel
actor Actor {initializeActor, tick} =
  Orvis.Program
    { Orvis.initialize = initialize'
    , Orvis.update = update'
    , Orvis.represent = id
    }
  where
    initialPhysicsCommand flags =
      let initializedActor = initializeActor flags
       in Cmd $
          Initialized
            (initialPhysics initializedActor)
            (initialForce initializedActor)
    initialize' flags =
      let initializedActor = initializeActor flags
       in ( Model
              { dead = False
              , physics = initialPhysics initializedActor
              , actorType = fixedActorType initializedActor
              , custom = initialCustomState initializedActor
              }
          , initialPhysicsCommand flags <> initialCommand initializedActor)
    tickWithDeathNotification customFlags input =
      addDeathNotificationToTickResult . tick customFlags input
    update' customFlags input model =
      case input of
        SelfDestruct -> (model {dead = True}, Die [Dead])
        PhysicsUpdate kin ->
          tickWithDeathNotification customFlags Nothing $
          updateKinematics kin model
        NoPhysicsUpdateTick ->
          tickWithDeathNotification customFlags Nothing model
        ActorSpecificInput command ->
          tickWithDeathNotification customFlags (Just command) model

addDeathNotificationToTickResult ::
     (Model customModel, Cmd (Output customOutput))
  -> (Model customModel, Cmd (Output customOutput))
addDeathNotificationToTickResult (state, cmd) =
  case cmd of
    Die _ -> (state, cmd <> Cmd Dead)
    otherCmd -> (state, otherCmd)
