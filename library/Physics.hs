module Physics
  ( Bounds(..)
  , EvaluatedWorld(..)
  , Force(..)
  , Physics(..)
  , Kinematics(..)
  ) where

import Data.Map.Strict (Map)
import Linear.V2 (V2(..))
import Orvis.Unique (Id)

data Physics = Physics
  { bounds :: Bounds
  , kinematics :: Kinematics
  , mass :: Double
  , frictionCoefficient :: Double
  } deriving (Show)

newtype EvaluatedWorld =
  EvaluatedWorld (Map Id Kinematics)
  deriving (Show)

newtype Bounds = Circular
  { radius :: Double
  } deriving (Show)

data Kinematics = Kinematics
  { position :: V2 Double
  , velocity :: V2 Double
  , rotation :: Double
  } deriving (Show)

newtype Force =
  Force (V2 Double)
  deriving (Show)
