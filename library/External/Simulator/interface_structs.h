#include "../../../Chipmunk-7.0.2/include/chipmunk/chipmunk.h"

typedef struct xyzKinematics
{
    cpVect position;
    cpVect velocity;
    cpFloat rotation;
} xyzKinematics;