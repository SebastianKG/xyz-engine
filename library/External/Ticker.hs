{-# LANGUAGE NamedFieldPuns #-}

module External.Ticker
  ( Tick(..)
  , TickerFlags(..)
  , ticker
  ) where

import Control.Concurrent (threadDelay)
import Control.Monad (forever)
import Control.Monad.IO.Class (liftIO)
import Orvis.Adapter.Outflow (Broadcaster(..))
import Orvis.Flags (Flags(..))
import Orvis.Port.Program (CleanUp(..), Program(..))

newtype TickerFlags = TickerFlags
  { microsecondsPerTick :: Int
  }

data Tick =
  Tick

ticker :: Program TickerFlags () () Tick
ticker =
  Program
    { initialize = const $ return ((), CleanUp {doCleanUp = return ()})
    , run =
        \(Flags identity TickerFlags {microsecondsPerTick}) () _ Broadcaster {send} ->
          liftIO $
          forever $ do
            threadDelay microsecondsPerTick
            send identity Tick
    }
