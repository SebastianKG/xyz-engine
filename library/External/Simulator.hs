{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module External.Simulator
  ( Input(..)
  , EntityInput(..)
  , Output(..)
  , SimulatorFlags(..)
  , simulator
  ) where

import Physics
  ( Bounds(..)
  , EvaluatedWorld(..)
  , Force(..)
  , Kinematics(..)
  , Physics(..)
  )

import Control.Concurrent.STM (TVar, atomically, modifyTVar, newTVar, readTVar)
import Control.Monad (forM_, forever)
import Control.Monad.IO.Class (liftIO)
import qualified Data.Map as Map
import Data.Map (Map)
import Data.Traversable (for)
import Foreign.Ptr (Ptr)
import Foreign.Storable (Storable(..))
import Language.C.Inline (CDouble(..), block)
import qualified Language.C.Inline as C
import qualified Language.C.Inline.Context.Chipmunk2d as Chipmunk2d
import Linear.V2 (V2(..))
import Orvis.Adapter.Outflow (Broadcaster(..))
import Orvis.Flags (Flags(..))
import Orvis.Port.Program (CleanUp(..), Program(..), Subscription(..))
import Orvis.Topology (Topology)
import Orvis.Unique (Id)

C.context Chipmunk2d.ctx

C.include "<stdio.h>"

C.include "<chipmunk.h>"

C.include "Simulator/interface_structs.h"

data Input
  = Tick
  | EntityAction Id
                 EntityInput

data EntityInput
  = Initialize Physics
               Force
  | ApplyForce Force
  | ApplyAngularVelocity Double
  | Remove

data Output
  = SimulationResult EvaluatedWorld
  | CurrentRoom Double
                Double

data SpaceMetadata = SpaceMetadata
  { spaceReference :: Ptr Chipmunk2d.Space
  , walls :: SquareWalls
  }

data SquareWalls = SquareWalls
  { left :: Ptr Chipmunk2d.Shape
  , top :: Ptr Chipmunk2d.Shape
  , right :: Ptr Chipmunk2d.Shape
  , bottom :: Ptr Chipmunk2d.Shape
  }

data World = World
  { space :: SpaceMetadata
  , entities :: Map Id Entity
  }

data Entity = Entity
  { controller :: Ptr Chipmunk2d.Body
  , body :: Ptr Chipmunk2d.Body
  , internal :: EntityInternal
  , appliedForce :: Maybe Force
  , appliedAngularVelocity :: Maybe Double
  }

data EntityInternal = EntityInternal
  { controlConstraint :: Ptr Chipmunk2d.Constraint
  , shape :: Ptr Chipmunk2d.Shape
  }

data SimulatorFlags = SimulatorFlags
  { roomWidth :: Double
  , roomHeight :: Double
  }

newtype InitialState =
  InitialState (TVar World)

simulator :: Program SimulatorFlags InitialState Input Output
simulator = Program {initialize = initialize', run = run'}

initialize' :: Flags SimulatorFlags -> Topology (InitialState, CleanUp)
initialize' (Flags _ customFlags) =
  liftIO $ do
    world <- initializeWorld customFlags
    return (InitialState world, CleanUp {doCleanUp = liftIO $ freeWorld world})

run' ::
     Flags SimulatorFlags
  -> InitialState
  -> Subscription Input
  -> Broadcaster Output
  -> Topology ()
run' (Flags myId customFlags) (InitialState world) subscription Broadcaster {send} =
  liftIO $
  case subscription of
    NotSubscribed -> return ()
    Subscription {next} ->
      forever $ do
        message <- next
        World { entities = currentEntities
              , space = SpaceMetadata {spaceReference}
              } <- atomically $ readTVar world
        case message of
          Tick -> do
            currentKinematics <-
              currentEntities `for` \Entity { body
                                            , appliedForce
                                            , appliedAngularVelocity
                                            } -> do
                kinematics <- kinematicsForBody body
                forM_ appliedForce (applyForceToBody body)
                case appliedAngularVelocity of
                  Just t -> applyAngularVelocityToBody body t
                  Nothing -> applyAngularVelocityToBody body 0
                return kinematics
            let SimulatorFlags {roomWidth, roomHeight} = customFlags
            send myId $ CurrentRoom roomWidth roomHeight
            send myId $ SimulationResult $ EvaluatedWorld currentKinematics
            tickSpace spaceReference
            atomically $
              modifyTVar world $ \currentWorld ->
                currentWorld
                  {entities = fmap clearEntity . entities $ currentWorld}
          EntityAction identity entityMessage ->
            case entityMessage of
              Initialize physics initialForce -> do
                newEntity <-
                  initializeEntity spaceReference physics initialForce
                atomically $
                  modifyTVar world $ \currentWorld ->
                    currentWorld
                      { entities =
                          Map.insert identity newEntity $ entities currentWorld
                      }
              ApplyForce force ->
                atomically $
                modifyTVar world $ \currentWorld ->
                  currentWorld
                    { entities =
                        Map.alter (recordForce force) identity $
                        entities currentWorld
                    }
              ApplyAngularVelocity torque ->
                atomically $
                modifyTVar world $ \currentWorld ->
                  currentWorld
                    { entities =
                        Map.alter (recordAngularVelocity torque) identity $
                        entities currentWorld
                    }
              Remove -> do
                let entity = Map.lookup identity currentEntities
                entity `forM_` freeEntity spaceReference
                atomically $
                  modifyTVar world $ \currentWorld ->
                    currentWorld
                      {entities = Map.delete identity $ entities currentWorld}

recordForce :: Force -> Maybe Entity -> Maybe Entity
recordForce force entity = do
  e <- entity
  return $ e {appliedForce = pure force}

recordAngularVelocity :: Double -> Maybe Entity -> Maybe Entity
recordAngularVelocity torque entity = do
  e <- entity
  return $ e {appliedAngularVelocity = pure torque}

clearEntity :: Entity -> Entity
clearEntity e = e {appliedForce = Nothing, appliedAngularVelocity = Nothing}

initializeWorld :: SimulatorFlags -> IO (TVar World)
initializeWorld flags = do
  emptySpace <- initializeSpace flags
  atomically $ newTVar World {space = emptySpace, entities = Map.empty}

freeWorld :: TVar World -> IO ()
freeWorld world = do
  World {space = SpaceMetadata {spaceReference, walls}, entities} <-
    atomically $ readTVar world
  mapM_ (freeEntity spaceReference) entities
  let SquareWalls {left, top, right, bottom} = walls
  freeShape spaceReference left
  freeShape spaceReference top
  freeShape spaceReference right
  freeShape spaceReference bottom
  [block| void {
    cpSpace* space = $(cpSpace* spaceReference);
    cpSpaceFree(space);
  } |]

fromChipmunkKinematics :: Chipmunk2d.Kinematics -> Kinematics
fromChipmunkKinematics Chipmunk2d.Kinematics { Chipmunk2d.position = pos
                                             , Chipmunk2d.velocity = vel
                                             , Chipmunk2d.rotation = rot
                                             } =
  let normalize :: Chipmunk2d.Vect -> V2 Double
      normalize Chipmunk2d.Vect {Chipmunk2d.x = x, Chipmunk2d.y = y} = V2 x y
   in Kinematics
        {position = normalize pos, velocity = normalize vel, rotation = rot}

initializeSpace :: SimulatorFlags -> IO SpaceMetadata
initializeSpace flags = do
  space <-
    [block| cpSpace* {
      return cpSpaceNew();
    } |]
  staticBody <-
    [block| cpBody* {
      cpSpace* space = $(cpSpace* space);
      return cpSpaceGetStaticBody(space);
    } |]
  let wallThickness = 100
      initializeWall :: V2 CDouble -> V2 CDouble -> IO (Ptr Chipmunk2d.Shape)
      initializeWall (V2 x1 y1) (V2 x2 y2) =
        [block| cpShape* {
          cpSpace* space = $(cpSpace* space);
          cpBody* staticBody = $(cpBody* staticBody);
          cpFloat wallThickness = $(double wallThickness);
          cpVect v1 = cpv($(double x1), $(double y1));
          cpVect v2 = cpv($(double x2), $(double y2));
          cpShape* shape = cpSpaceAddShape(
            space,
            cpSegmentShapeNew(staticBody, v1, v2, wallThickness)
          );
          cpShapeSetElasticity(shape, 1.0);
          cpShapeSetFriction(shape, 1.0);
          return shape;
        } |]
      SimulatorFlags {roomWidth, roomHeight} = flags
      verticalWallOffset = V2 0 wallThickness
      horizontalWallOffset = V2 wallThickness 0
      halfWidth = CDouble roomWidth / 2
      halfHeight = CDouble roomHeight / 2
      topRight = V2 halfWidth halfHeight
      topLeft = V2 (negate halfWidth) halfHeight
      bottomRight = V2 halfWidth (negate halfHeight)
      bottomLeft = V2 (negate halfWidth) (negate halfHeight)
  left <-
    initializeWall
      (bottomLeft - verticalWallOffset - horizontalWallOffset)
      (topLeft + verticalWallOffset - horizontalWallOffset)
  right <-
    initializeWall
      (bottomRight - verticalWallOffset + horizontalWallOffset)
      (topRight + verticalWallOffset + horizontalWallOffset)
  bottom <-
    initializeWall
      (bottomLeft - horizontalWallOffset - verticalWallOffset)
      (bottomRight + horizontalWallOffset - verticalWallOffset)
  top <-
    initializeWall
      (topLeft - horizontalWallOffset + verticalWallOffset)
      (topRight + horizontalWallOffset + verticalWallOffset)
  return SpaceMetadata {spaceReference = space, walls = SquareWalls {..}}

freeEntity :: Ptr Chipmunk2d.Space -> Entity -> IO ()
freeEntity space Entity {controller, body, internal} =
  let EntityInternal {shape, controlConstraint} = internal
   in do freeConstraint space controlConstraint
         freeBody space controller
         freeShape space shape
         freeBody space body

freeConstraint :: Ptr Chipmunk2d.Space -> Ptr Chipmunk2d.Constraint -> IO ()
freeConstraint space constraint =
  [block| void {
    cpSpace* space = $(cpSpace* space);
    cpConstraint* constraint = $(cpConstraint* constraint);

    cpSpaceRemoveConstraint(space, constraint);
    cpConstraintFree(constraint);
  } |]

freeShape :: Ptr Chipmunk2d.Space -> Ptr Chipmunk2d.Shape -> IO ()
freeShape space shape =
  [block| void {
    cpSpace* space = $(cpSpace* space);
    cpShape* shape = $(cpShape* shape);

    cpSpaceRemoveShape(space, shape);
    cpShapeFree(shape);
  } |]

freeBody :: Ptr Chipmunk2d.Space -> Ptr Chipmunk2d.Body -> IO ()
freeBody space body =
  [block| void {
    cpSpace* space = $(cpSpace* space);
    cpBody* body = $(cpBody* body);

    cpSpaceRemoveBody(space, body);
    cpBodyFree(body);
  } |]

tickSpace :: Ptr Chipmunk2d.Space -> IO ()
tickSpace space =
  [block| void {
    cpSpace* space = $(cpSpace* space);
    cpSpaceStep( space, 1.0/60.0 );
  } |]

initializeEntity :: Ptr Chipmunk2d.Space -> Physics -> Force -> IO Entity
initializeEntity space physics (Force initialForce) =
  let V2 initialXForce initialYForce = CDouble <$> initialForce
      Physics {kinematics = kin, mass, frictionCoefficient, bounds} = physics
      coefficient = CDouble frictionCoefficient
      Circular {radius} = bounds
      circleRadius = CDouble radius
      Kinematics {position} = kin
      entityMass = CDouble mass
      V2 x y = CDouble <$> position
   in do controlBody <-
           [block| cpBody* {
            cpSpace* space = $(cpSpace* space);
            return cpSpaceAddBody(space, cpBodyNewKinematic());
           } |]
         physicalBody <-
           [block| cpBody* {
            cpFloat radius = $(double circleRadius);
            cpFloat mass = $(double entityMass);
            cpVect position = cpv( $(double x), $(double y) );
            cpVect initialForce = cpv(
              $(double initialXForce),
              $(double initialYForce)
            );
            cpSpace* space = $(cpSpace* space);

            cpFloat moment = cpMomentForCircle(mass, 0, radius, cpvzero);
            cpBody* ballBody = cpSpaceAddBody(space, cpBodyNew(mass, moment));
            cpBodySetPosition(ballBody, position);
            cpBodySetVelocity(ballBody, initialForce);
            return ballBody;
           } |]
         let maxForce = coefficient * 10000
         simulatedFrictionConstraint <-
           [block| cpConstraint* {
            cpBody* controlBody = $(cpBody* controlBody);
            cpBody* physicalBody = $(cpBody* physicalBody);
            cpFloat maxForce = $(double maxForce);
            cpSpace* space = $(cpSpace* space);
            cpConstraint* pivot = cpSpaceAddConstraint(
              space, 
              cpPivotJointNew2(controlBody, physicalBody, cpvzero, cpvzero)
            );
            cpConstraintSetMaxBias(pivot, 0);
            cpConstraintSetMaxForce(pivot, maxForce);
            return pivot;
           } |]
         boundaryShape <-
           [block| cpShape* {
            cpFloat radius = $(double circleRadius);
            cpFloat coefficient = $(double coefficient);
            cpSpace* space = $(cpSpace* space);
            cpBody* body = $(cpBody* physicalBody);

            cpShape* ballShape = cpSpaceAddShape(
              space, 
              cpCircleShapeNew(body, radius, cpvzero)
            );
            cpShapeSetFriction(ballShape, coefficient);
            return ballShape;
           } |]
         return
           Entity
             { controller = controlBody
             , body = physicalBody
             , internal =
                 EntityInternal
                   { controlConstraint = simulatedFrictionConstraint
                   , shape = boundaryShape
                   }
             , appliedForce = Nothing
             , appliedAngularVelocity = Nothing
             }

applyForceToBody :: Ptr Chipmunk2d.Body -> Force -> IO ()
applyForceToBody body (Force forceVector) =
  let V2 fx fy = CDouble <$> forceVector
   in [block| void {
        cpBody* body = $(cpBody* body);
        cpBodySetForce(
            body,
            cpv(
              $(double fx),
              $(double fy)
            )
          );
      } |]

applyAngularVelocityToBody :: Ptr Chipmunk2d.Body -> Double -> IO ()
applyAngularVelocityToBody body torque =
  let torqueToApply = CDouble torque
   in [block| void { 
        cpBodySetAngularVelocity(
          $(cpBody* body),
          $(double torqueToApply)
        );
      } |]

kinematicsForBody :: Ptr Chipmunk2d.Body -> IO Kinematics
kinematicsForBody body = do
  kinematics <-
    [block| xyzKinematics* {
      cpBody* body = $(cpBody* body);

      xyzKinematics* report = malloc(
        sizeof(xyzKinematics)
      );
      report->position = cpBodyGetPosition(body);
      report->velocity = cpBodyGetVelocity(body);
      report->rotation = cpBodyGetAngle(body);
      return report;
    } |]
  kin <- peek kinematics
  [block| void { 
    free( $(xyzKinematics* kinematics) );
  } |]
  return $ fromChipmunkKinematics kin
