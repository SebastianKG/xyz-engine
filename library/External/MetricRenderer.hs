{-# LANGUAGE NamedFieldPuns #-}

module External.MetricRenderer
  ( Output(..)
  , MetricRendererFlags(..)
  , metricRenderer
  ) where

import qualified API.WebSocket.ServerToClient as ServerToClient
import Control.Concurrent (threadDelay)
import Control.Monad (forever)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader.Class (asks)
import Data.ByteString.Lazy (ByteString)
import qualified Data.Map.Strict as Map
import Orvis.Adapter.Outflow (Broadcaster(..))
import Orvis.Flags (Flags(..))
import qualified Orvis.Metrics
import Orvis.Port.Program (CleanUp(..), Program(..))
import qualified Orvis.Topology

newtype Output =
  Rendered ByteString

newtype MetricRendererFlags = MetricRendererFlags
  { microsecondsPerTick :: Int
  }

metricRenderer :: Program MetricRendererFlags () () Output
metricRenderer =
  Program
    { initialize = const $ return ((), CleanUp {doCleanUp = return ()})
    , run =
        \(Flags identity MetricRendererFlags {microsecondsPerTick}) () _ Broadcaster {send} -> do
          metricsTracker <- asks Orvis.Topology.metrics
          liftIO $
            forever $ do
              threadDelay microsecondsPerTick
              mailboxMap <- Orvis.Metrics.getMailboxes metricsTracker
              portMap <- Orvis.Metrics.getPorts metricsTracker
              robotMap <- Orvis.Metrics.getRobots metricsTracker
              let metricsUpdate =
                    ServerToClient.MetricsUpdate
                      { ServerToClient.mailboxes =
                          Map.elems $
                          ServerToClient.representMailboxMetrics `Map.mapWithKey`
                          mailboxMap
                      , ServerToClient.ports =
                          Map.elems $
                          ServerToClient.representPortMetrics `Map.mapWithKey`
                          portMap
                      , ServerToClient.robots =
                          Map.elems $
                          ServerToClient.representRobotMetrics `Map.mapWithKey`
                          robotMap
                      }
              send identity $
                Rendered $
                ServerToClient.serialize $ ServerToClient.Metrics metricsUpdate
    }
