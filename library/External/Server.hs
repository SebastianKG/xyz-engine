{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}

module External.Server
  ( run
  , GameRules(..)
  ) where

import qualified API.WebSocket.ClientToServer as ClientToServer
import qualified API.WebSocket.ServerToClient as ServerToClient
import Control.Concurrent (forkIO)
import Control.Concurrent (ThreadId)
import Control.Concurrent.STM.TVar
  ( TVar
  , modifyTVar
  , newTVar
  , readTVar
  , swapTVar
  , writeTVar
  )
import Control.Exception (Exception, SomeException, catch, throw)
import Control.Monad (forM, forever, unless, void, when)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader (ask)
import Control.Monad.STM (STM, atomically)
import Data.List (find, sortBy)
import qualified External.MetricRenderer as MetricRenderer
import External.MetricRenderer
  ( MetricRendererFlags(MetricRendererFlags)
  , metricRenderer
  )
import qualified Game
import Game (GameFlags(..))
import Network.Wai (Application)
import qualified Network.Wai.Handler.Warp as Warp
import Network.Wai.Handler.WebSockets (websocketsOr)
import Network.Wai.Middleware.Static (static)
import Network.WebSockets (ServerApp, forkPingThread, receiveData, sendTextData)
import Network.WebSockets.Connection
  ( acceptRequest
  , defaultConnectionOptions
  , rejectRequest
  )
import Orvis.Adapter.Outflow (Outflow, outflow)
import Orvis.Mailbox (Message(..))
import qualified Orvis.Mailbox as Mailbox
import qualified Orvis.Port as Port
import Orvis.Port (Port(Port))
import Orvis.Topology (Tools(..), Topology, counter, topology, topologyWith)
import Orvis.Unique (Id, getNext, raw)
import qualified Orvis.Unique as Unique
import Web.Scotty (file, get, scottyApp, setHeader)

run :: GameRules -> IO ()
run rules = do
  contentServer <- buildContentServer
  topology $ do
    tools <- ask
    let c = counter tools
    myId <- liftIO $ getNext c
    games <- makeEmptyGames rules
    liftIO $
      Warp.run 8080 $
      websocketsOr
        defaultConnectionOptions
        (websocketServer rules tools games myId (getNext c))
        contentServer

websocketServer ::
     GameRules -> Tools -> TVar [RunningGame] -> Id -> IO Id -> ServerApp
websocketServer rules tools games myId getUniqueId pending = do
  potentialGame <- atomically $ pickGame rules games
  gameList <- atomically $ readTVar games
  logGameList gameList
  case potentialGame of
    Nothing -> do
      putStrLn "Failed to join a game"
      rejectRequest pending ""
    Just game@RunningGame {identity = gameId} -> do
      connection <- acceptRequest pending
      hasJoined <- atomically $ newTVar False
      proposedPlayerId <- getUniqueId
      putStrLn $
        concat
          ["Joining Game ", show gameId, " as Player ", show proposedPlayerId]
      let Game.ExternalMailboxes { Game.externalWebSocketMessagesToSend = externalWebSocketMessagesToSend
                                 , Game.externalPlayerInput = externalPlayerInput
                                 , Game.externalNewPlayerRequests = externalNewPlayerRequests
                                 } = mailboxes game
      renderOutbox <- Mailbox.subscribe externalWebSocketMessagesToSend
      let inputInbox = Mailbox.open externalPlayerInput
          playerRequestInbox = Mailbox.open externalNewPlayerRequests
          sendPlayerRequest :: Game.PlayerSpawnRequest -> IO ()
          sendPlayerRequest spawnRequest =
            Mailbox.send playerRequestInbox $
            Message {identity = myId, message = spawnRequest}
          sendInput :: Game.PlayerInput -> IO ()
          sendInput playerInput =
            Mailbox.send inputInbox $
            Message {identity = myId, message = playerInput}
          handleDisconnection :: SomeException -> IO a
          handleDisconnection _ = do
            sendInput $
              Game.PlayerInput proposedPlayerId ClientToServer.Disconnected
            shouldCreateNewGame <-
              atomically $ do
                currentGameList <- readTVar games
                let (newGameList, needNewGame) =
                      leaveGame gameId currentGameList
                writeTVar games newGameList
                pure needNewGame
            when shouldCreateNewGame $ do
              newGame <- startGame rules tools
              atomically $
                modifyTVar games $ \currentGames -> newGame : currentGames
            throw PlayerDisconnected
          handleInput :: ClientToServer.Update -> IO ()
          handleInput i =
            case i of
              ClientToServer.JoinGame -> do
                joined <- atomically $ readTVar hasJoined
                unless joined $ do
                  sendPlayerRequest $ Game.SpawnPlayerWithId proposedPlayerId
                  atomically $ writeTVar hasJoined True
                  sendTextData connection $
                    ServerToClient.serialize $
                    ServerToClient.BeginGame
                      ServerToClient.StartingInfo
                        { ServerToClient.playerIdentity =
                            fromIntegral $ raw proposedPlayerId
                        }
                  void $
                    forkIO $
                    void $
                    forever $ do
                      Message {message = serialized} <-
                        Mailbox.next renderOutbox
                      sendTextData connection serialized
              ClientToServer.FromUser cInput ->
                sendInput $ Game.PlayerInput proposedPlayerId cInput
      void $ do
        forkPingThread connection 20
        forever $ do
          input <- receiveData connection `catch` handleDisconnection
          let parsed = ClientToServer.deserialize input
          either putStrLn handleInput parsed

data PlayerDisconnected =
  PlayerDisconnected
  deriving (Show)

instance Exception PlayerDisconnected

buildContentServer :: IO Application
buildContentServer = do
  app <-
    scottyApp $
    get "/" $ do
      setHeader "Content-Type" "text/html; charset=utf-8"
      file "static/index.html"
  return $ static app

data GameRules = GameRules
  { maxPlayerCount :: Int
  , gamesPerServer :: Int
  , framesPerSecond :: Int
  , withMetrics :: Bool
  , lockWhenFull :: Bool
  , gameLogic :: Game.Mailboxes -> Topology ()
  }

data PlayerCount = PlayerCount
  { currentCount :: Int
  , outOf :: Int
  }

data RunningGame = RunningGame
  { identity :: Id
  , mailboxes :: Game.ExternalMailboxes
  , players :: PlayerCount
  , locked :: Bool
  , gameThread :: ThreadId
  }

startGame :: GameRules -> Tools -> IO RunningGame
startGame GameRules {gameLogic, maxPlayerCount, framesPerSecond} tools@Tools {counter} = do
  topologyWith tools $ do
    externalWebSocketMessagesToSend <-
      Mailbox.spawn "WebSocket Messages to Send"
    externalPlayerInput <- Mailbox.spawn "Player Input"
    externalNewPlayerRequests <- Mailbox.spawn "New Player Requests"
    let externalMailboxes = Game.ExternalMailboxes {..}
    gameId <- liftIO $ Unique.getNext counter
    threadId <-
      liftIO $
      forkIO $
      topologyWith tools $
      Game.start
        GameFlags
          { externalMailboxes
          , gameLogic
          , microsecondsPerTick =
              round $ (1000000 :: Double) / (fromIntegral framesPerSecond)
          }
    pure $
      RunningGame
        { identity = gameId
        , mailboxes = externalMailboxes
        , players = PlayerCount {currentCount = 0, outOf = maxPlayerCount}
        , locked = False
        , gameThread = threadId
        }

makeEmptyGames :: GameRules -> Topology (TVar [RunningGame])
makeEmptyGames rules@GameRules {gamesPerServer, withMetrics} = do
  tools <- ask
  games <- liftIO $ forM [1 .. gamesPerServer] $ \_ -> startGame rules tools
  when withMetrics $
    -- metric renderer
    Port.spawn
      "Metric Renderer"
      MetricRendererFlags
        { MetricRenderer.microsecondsPerTick = 166667 -- 6 Hz
        }
      Port
        { Port.program = metricRenderer
        , Port.subscriptionPoints = []
        , Port.broadcastPoints = connectToWebSocketMessageMailbox <$> games
        }
  liftIO $ atomically $ newTVar games

connectToWebSocketMessageMailbox :: RunningGame -> Outflow MetricRenderer.Output
connectToWebSocketMessageMailbox RunningGame {mailboxes} =
  let Game.ExternalMailboxes {Game.externalWebSocketMessagesToSend = mailbox} =
        mailboxes
   in mailbox `outflow` \Message {message = MetricRenderer.Rendered content} ->
        pure content

pickGame :: GameRules -> TVar [RunningGame] -> STM (Maybe RunningGame)
pickGame rules games = do
  gameList <- readTVar games
  let game =
        find joinableGame $
        sortBy
          (\RunningGame {identity = firstId} RunningGame {identity = secondId} ->
             Unique.raw firstId `compare` Unique.raw secondId)
          gameList
  case game of
    Nothing -> return Nothing
    Just joinedGame@RunningGame {identity} -> do
      void $ swapTVar games $ modifyGame identity (joinGame rules) gameList
      return $ pure joinedGame

joinableGame :: RunningGame -> Bool
joinableGame RunningGame {players, locked} =
  let PlayerCount {currentCount, outOf} = players
   in currentCount < outOf && not locked

modifyGame ::
     Id -> (RunningGame -> RunningGame) -> [RunningGame] -> [RunningGame]
modifyGame gameId modification games =
  (\game@RunningGame {identity} ->
     if gameId == identity
       then modification game
       else game) <$>
  games

joinGame :: GameRules -> RunningGame -> RunningGame
joinGame GameRules {lockWhenFull} game@RunningGame {players, locked} =
  let count@PlayerCount {currentCount, outOf} = players
   in if currentCount < outOf && not locked
        then game
               { players = count {currentCount = currentCount + 1}
               , locked =
                   lockWhenFull && not locked && currentCount + 1 == outOf
               }
        else game

leaveGame :: Id -> [RunningGame] -> ([RunningGame], Bool)
leaveGame myGameId =
  let handleLeave game@RunningGame {players, locked, identity} (games, needNewGame) =
        let count@PlayerCount {currentCount} = players
            newCount = max (currentCount - 1) 0
            shouldDeleteGame = newCount <= 0 && locked
         in if currentCount <= 0 || identity /= myGameId
              then (game : games, needNewGame) -- either the game can't be left, or it's not the one we were looking for
              else if shouldDeleteGame
                     then (games, True) -- get rid of the current game and request a new one
                     else ( game {players = count {currentCount = newCount}} : -- decrement the player count for this game
                            games
                          , needNewGame)
   in foldr handleLeave ([], False)

logGameList :: [RunningGame] -> IO ()
logGameList gameList =
  putStrLn $
  unlines $
  fmap
    (\RunningGame { identity
                  , locked
                  , players = PlayerCount {currentCount, outOf}
                  } ->
       concat
         [ " | Game "
         , show identity
         , ": "
         , take (4 - length (show $ Unique.raw identity)) $ repeat ' '
         , show currentCount
         , "/"
         , show outOf
         , " | "
         , if locked
             then "<- LOCKED"
             else ""
         ]) $
  filter
    (\RunningGame {players = PlayerCount {currentCount}} -> currentCount > 0)
    gameList
