#!/bin/bash
set -e

# colors for use in this script
ERRORCOLOR='\033[1;31m'
COLOR1='\033[1;33m'
COLOR2='\033[1;34m'
COLOR3='\033[1;35m'
COLOR4='\033[1;36m'
CLEAR='\033[0m'

function cleanup {
  echo -e "${ERRORCOLOR}[!!!] The build failed.${CLEAR}"
}
trap cleanup EXIT

# ensure existence of static directories
mkdir -p static/compiled
mkdir -p client/Communication/WebSocket/Compiled

echo -e "${COLOR1}[1/4] Compiling xyz-engine and xyz-elm-bridge.${CLEAR}"
stack install --extra-include-dirs=/usr/local/include/chipmunk --extra-lib-dirs=/usr/local/lib/ --pedantic

echo -e "${COLOR2}[2/4] Compiling bridging types for Elm.${CLEAR}"
xyz-elm-bridge

echo -e "${COLOR3}[3/4] Compiling client JavaScript from Elm source code.${CLEAR}"
# (cd client && elm make Main.elm --optimize --output ../static/compiled/client.js)
(cd client && elm make Main.elm --output ../static/compiled/client.js)

# run xyz-engine
if [ "$1" ]; then
  echo -e "${COLOR4}[4/4] Running $1!${CLEAR}"
  xyz-engine --game="$1"
else
  echo -e "${COLOR4}[4/4] Running!${CLEAR}"
  stack exec -- xyz-engine
fi