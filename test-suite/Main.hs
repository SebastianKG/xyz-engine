{-# LANGUAGE NamedFieldPuns #-}

import qualified Test.Tasty

import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader.Class (asks)
import qualified Data.Set as Set
import Entity.NestingGame
  ( BoardState(..)
  , Color(..)
  , DiagonalDirection(..)
  , GameState(..)
  , HangingPiece(..)
  , Input(..)
  , Lobby(..)
  , Model(..)
  , Piece(..)
  , PlayerPieceCounts(..)
  , Row(..)
  , WinningMethod(..)
  , emptyRow
  , nestingGame
  , newGame
  )
import Linear.V2 (V2(..))
import Orvis.Robot.Program (Program(..))
import Orvis.Topology (counter, topology)
import Orvis.Unique (getNext)
import Test.Tasty.Hspec

main :: IO ()
main = do
  test <- testSpec "xyz-engine" spec
  Test.Tasty.defaultMain test

spec :: Spec
spec =
  let Program {update} = nestingGame
      up input model = fst $ update () input model
      getNewId = do
        c <- asks counter
        liftIO $ getNext c
      lobby = Loading . Lobby
      getPlayerIds = (,) <$> getNewId <*> getNewId
   in parallel $ do
        it "should add a player from empty loading screen" $ do
          topology $ do
            pid <- getNewId
            liftIO $ up (Join pid) (lobby []) `shouldBe` lobby [pid]
        it "should take away a player from non-empty loading screen" $ do
          topology $ do
            pid <- getNewId
            liftIO $ up (Leave pid) (lobby [pid]) `shouldBe` lobby []
        it "should start a game when two players have joined" $ do
          topology $ do
            (p1, p2) <- getPlayerIds
            liftIO $
              up (Join p1) (lobby [p2]) `shouldBe` InProgress (newGame p1 p2)
        it "should place a piece in a started game" $ do
          topology $ do
            (blackId, whiteId) <- getPlayerIds
            let defaultStartedGame = newGame blackId whiteId
                startingGame@GameState {blackPieces} =
                  defaultStartedGame {currentTurn = Black}
            liftIO $
              up (Place (Smaller blackId) (V2 0 0)) (InProgress startingGame) `shouldBe`
              InProgress
                (startingGame
                   { board =
                       BoardState
                         [ Row
                             [ Set.fromList [Smaller Black]
                             , Set.empty
                             , Set.empty
                             , Set.empty
                             ]
                         , emptyRow
                         , emptyRow
                         , emptyRow
                         ]
                   , blackPieces = blackPieces {smaller = 2}
                   , currentTurn = White
                   })
        it "should move a piece from one position to another" $ do
          topology $ do
            (blackId, whiteId) <- getPlayerIds
            let startingGame@GameState {whitePieces} = newGame blackId whiteId
                modifiedGame =
                  startingGame
                    { board =
                        BoardState
                          [ emptyRow
                          , Row
                              [ Set.empty
                              , Set.empty
                              , Set.fromList [Biggest White]
                              , Set.empty
                              ]
                          , emptyRow
                          , emptyRow
                          ]
                    , whitePieces = whitePieces {biggest = 2}
                    }
            liftIO $
              up (Move whiteId (V2 2 1) (V2 0 1)) (InProgress modifiedGame) `shouldBe`
              InProgress
                (startingGame
                   { board =
                       BoardState
                         [ emptyRow
                         , Row
                             [ Set.fromList [Biggest White]
                             , Set.empty
                             , Set.empty
                             , Set.empty
                             ]
                         , emptyRow
                         , emptyRow
                         ]
                   , whitePieces = whitePieces {biggest = 2}
                   , currentTurn = Black
                   })
        it "should not change when place command is invalid" $
          topology $ do
            (blackId, whiteId) <- getPlayerIds
            let startingGame = newGame blackId whiteId
            liftIO $
              up (Place (Smaller whiteId) (V2 20 55)) (InProgress startingGame) `shouldBe`
              (InProgress $
               startingGame
                 { lastTurnError =
                     Just "Invalid coordinates (20, 55) for retrieving cell."
                 })
        it "should not change when move command is invalid" $
          topology $ do
            (blackId, whiteId) <- getPlayerIds
            let startingGame = newGame blackId whiteId
            liftIO $
              up (Move blackId (V2 2 30) (V2 15 1)) (InProgress startingGame) `shouldBe`
              (InProgress $ startingGame {lastTurnError = Just ""})
        it
          "should declare the game as won the moment the winning piece is placed" $
          topology $ do
            (blackId, whiteId) <- getPlayerIds
            let startingGame = newGame blackId whiteId
                modifiedGame =
                  startingGame
                    { board =
                        BoardState
                          [ Row
                              [ Set.fromList [Biggest White]
                              , Set.empty
                              , Set.empty
                              , Set.empty
                              ]
                          , Row
                              [ Set.empty
                              , Set.fromList [Bigger White]
                              , Set.empty
                              , Set.empty
                              ]
                          , Row
                              [ Set.empty
                              , Set.empty
                              , Set.fromList [Smaller White]
                              , Set.empty
                              ]
                          , emptyRow
                          ]
                    , whitePieces =
                        PlayerPieceCounts
                          {smallest = 3, smaller = 2, bigger = 2, biggest = 2}
                    }
            liftIO $
              up (Place (Smallest whiteId) (V2 3 3)) (InProgress modifiedGame) `shouldBe`
              WonBy
                whiteId
                startingGame
                  { board =
                      BoardState
                        [ Row
                            [ Set.fromList [Biggest White]
                            , Set.empty
                            , Set.empty
                            , Set.empty
                            ]
                        , Row
                            [ Set.empty
                            , Set.fromList [Bigger White]
                            , Set.empty
                            , Set.empty
                            ]
                        , Row
                            [ Set.empty
                            , Set.empty
                            , Set.fromList [Smaller White]
                            , Set.empty
                            ]
                        , Row
                            [ Set.empty
                            , Set.empty
                            , Set.empty
                            , Set.fromList [Smallest White]
                            ]
                        ]
                  , whitePieces =
                      PlayerPieceCounts
                        {smallest = 2, smaller = 2, bigger = 2, biggest = 2}
                  }
                (HangingPiece Nothing)
                (DiagonalLine TopLeftBottomRight)
        it
          "should declare the game as won the moment the winning piece is moved" $
          topology $ do
            (blackId, whiteId) <- getPlayerIds
            let blackPieceCounts =
                  PlayerPieceCounts
                    {smallest = 2, smaller = 3, bigger = 2, biggest = 1}
                whitePieceCounts =
                  PlayerPieceCounts
                    {smallest = 2, smaller = 2, bigger = 3, biggest = 1}
                startingGame = newGame blackId whiteId
                modifiedGame =
                  startingGame
                    { board =
                        BoardState
                          [ Row
                              [ Set.fromList [Biggest Black]
                              , Set.fromList [Biggest White]
                              , Set.empty
                              , Set.empty
                              ]
                          , Row
                              [ Set.fromList [Smaller White]
                              , Set.empty
                              , Set.fromList [Biggest Black]
                              , Set.empty
                              ]
                          , Row
                              [ Set.fromList [Smallest White]
                              , Set.fromList [Biggest White, Bigger Black]
                              , Set.fromList [Smaller Black]
                              , Set.empty
                              ]
                          , Row
                              [ Set.empty
                              , Set.empty
                              , Set.fromList [Smallest Black]
                              , Set.empty
                              ]
                          ]
                    , whitePieces = whitePieceCounts
                    , blackPieces = blackPieceCounts
                    , currentTurn = Black
                    }
            liftIO $
              up (Move blackId (V2 0 0) (V2 2 0)) (InProgress modifiedGame) `shouldBe`
              WonBy
                blackId
                startingGame
                  { board =
                      BoardState
                        [ Row
                            [ Set.empty
                            , Set.fromList [Biggest White]
                            , Set.fromList [Biggest Black]
                            , Set.empty
                            ]
                        , Row
                            [ Set.fromList [Smaller White]
                            , Set.empty
                            , Set.fromList [Biggest Black]
                            , Set.empty
                            ]
                        , Row
                            [ Set.fromList [Smallest White]
                            , Set.fromList [Biggest White, Bigger Black]
                            , Set.fromList [Smaller Black]
                            , Set.empty
                            ]
                        , Row
                            [ Set.empty
                            , Set.empty
                            , Set.fromList [Smallest Black]
                            , Set.empty
                            ]
                        ]
                  , whitePieces = whitePieceCounts
                  , blackPieces = blackPieceCounts
                  , currentTurn = Black
                  }
                (HangingPiece Nothing)
                (VerticalLine 2)
        it
          "should declare the game as won the moment the wrong piece is picked up" $
          topology $ do
            (blackId, whiteId) <- getPlayerIds
            let blackPieceCounts =
                  PlayerPieceCounts
                    {smallest = 2, smaller = 3, bigger = 2, biggest = 1}
                whitePieceCounts =
                  PlayerPieceCounts
                    {smallest = 1, smaller = 2, bigger = 2, biggest = 1}
                startingGame = newGame blackId whiteId
                modifiedGame =
                  startingGame
                    { board =
                        BoardState
                          [ Row
                              [ Set.fromList [Biggest Black, Bigger White]
                              , Set.fromList [Biggest White]
                              , Set.empty
                              , Set.empty
                              ]
                          , Row
                              [ Set.fromList [Smaller White]
                              , Set.empty
                              , Set.fromList [Biggest Black]
                              , Set.empty
                              ]
                          , Row
                              [ Set.fromList [Smallest White]
                              , Set.fromList [Biggest White, Bigger Black]
                              , Set.fromList [Smaller Black]
                              , Set.empty
                              ]
                          , Row
                              [ Set.fromList [Smallest White]
                              , Set.empty
                              , Set.fromList [Smallest Black]
                              , Set.empty
                              ]
                          ]
                    , whitePieces = whitePieceCounts
                    , blackPieces = blackPieceCounts
                    , currentTurn = Black
                    }
            liftIO $
              up (Move blackId (V2 0 0) (V2 2 0)) (InProgress modifiedGame) `shouldBe`
              WonBy
                whiteId
                startingGame
                  { board =
                      BoardState
                        [ Row
                            [ Set.fromList [Bigger White]
                            , Set.fromList [Biggest White]
                            , Set.empty
                            , Set.empty
                            ]
                        , Row
                            [ Set.fromList [Smaller White]
                            , Set.empty
                            , Set.fromList [Biggest Black]
                            , Set.empty
                            ]
                        , Row
                            [ Set.fromList [Smallest White]
                            , Set.fromList [Biggest White, Bigger Black]
                            , Set.fromList [Smaller Black]
                            , Set.empty
                            ]
                        , Row
                            [ Set.fromList [Smallest White]
                            , Set.empty
                            , Set.fromList [Smallest Black]
                            , Set.empty
                            ]
                        ]
                  , whitePieces = whitePieceCounts
                  , blackPieces = blackPieceCounts
                  , currentTurn = Black
                  }
                (HangingPiece $ pure $ Biggest Black)
                (VerticalLine 0)
        it "should declare the game as won with a diagonal win" $
          topology $ do
            (blackId, whiteId) <- getPlayerIds
            let blackPieceCounts =
                  PlayerPieceCounts
                    {smallest = 3, smaller = 3, bigger = 3, biggest = 3}
                whitePieceCounts =
                  PlayerPieceCounts
                    {smallest = 0, smaller = 3, bigger = 3, biggest = 3}
                startingGame = newGame blackId whiteId
                modifiedGame =
                  startingGame
                    { board =
                        BoardState
                          [ Row
                              [ Set.empty
                              , Set.empty
                              , Set.empty
                              , Set.fromList [Smallest White]
                              ]
                          , Row
                              [ Set.empty
                              , Set.empty
                              , Set.fromList [Smallest White]
                              , Set.empty
                              ]
                          , Row
                              [ Set.empty
                              , Set.fromList [Smallest White]
                              , Set.empty
                              , Set.empty
                              ]
                          , Row [Set.empty, Set.empty, Set.empty, Set.empty]
                          ]
                    , whitePieces = whitePieceCounts
                    , blackPieces = blackPieceCounts
                    , currentTurn = White
                    }
            liftIO $
              up (Place (Smaller whiteId) $ V2 0 3) (InProgress modifiedGame) `shouldBe`
              WonBy
                whiteId
                startingGame
                  { board =
                      BoardState
                        [ Row
                            [ Set.empty
                            , Set.empty
                            , Set.empty
                            , Set.fromList [Smallest White]
                            ]
                        , Row
                            [ Set.empty
                            , Set.empty
                            , Set.fromList [Smallest White]
                            , Set.empty
                            ]
                        , Row
                            [ Set.empty
                            , Set.fromList [Smallest White]
                            , Set.empty
                            , Set.empty
                            ]
                        , Row
                            [ Set.fromList [Smaller White]
                            , Set.empty
                            , Set.empty
                            , Set.empty
                            ]
                        ]
                  , whitePieces = whitePieceCounts {smaller = 2}
                  , blackPieces = blackPieceCounts
                  , currentTurn = White
                  }
                (HangingPiece Nothing)
                (DiagonalLine BottomLeftTopRight)
