module Main exposing (main)

import Browser exposing (Document)
import Browser.Dom
import Browser.Events
import Communication.WebSocket.ClientToServer as ClientToServer
import Communication.WebSocket.Compiled.ClientToServer as FromClient
import Communication.WebSocket.Compiled.ServerToClient as FromServer
import Communication.WebSocket.ServerToClient as ServerToClient
import ConnectionStatus exposing (ConnectionStatus(..))
import Control exposing (BoardLocation, GameLocation(..))
import Dict exposing (Dict)
import Entity exposing (Entity)
import Graphics
import Html exposing (..)
import Json.Decode exposing (Decoder)
import Json.Encode exposing (Value)
import Metrics exposing (Metrics)
import Msg exposing (Msg(..))
import PortFunnel.WebSocket as WebSocket exposing (Response(..))
import PortFunnels exposing (FunnelDict, Handler(..))
import Room exposing (Room)
import SystemInput exposing (Key(..), keyValue)
import Task
import Url exposing (Url)
import View.Default
import View.NestingGame
import View.OrvisTopology exposing (TopologyState)
import View.PhysicsWireframe
import Window


main : Program {} Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = always NoOp
        , onUrlChange = always NoOp
        }


type alias Model =
    { playerId : Maybe Int
    , entities : Dict Int Entity
    , room : Room
    , board : Maybe FromServer.GameModel
    , graphics : Graphics.Mode
    , window : Window.Size
    , websocketLocation : String
    , pendingWebSocketMessage : Maybe String
    , gameStarted : Bool
    , metrics : Metrics
    , topologyState : Maybe TopologyState
    , control : Maybe GameLocation
    , consideredLocation : Maybe GameLocation
    , portFunnelState : PortFunnels.State
    , connectionStatus : ConnectionStatus
    , gameCategory : GameCategory
    }


type GameCategory
    = BoardGame
    | OtherGame


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        Tick ->
            let
                tickedModel =
                    case model.topologyState of
                        Nothing ->
                            model

                        Just state ->
                            { model
                                | topologyState =
                                    Just <|
                                        View.OrvisTopology.tick
                                            model.metrics
                                            model.window
                                            state
                            }
            in
            ( if tickedModel.gameStarted then
                case tickedModel.pendingWebSocketMessage of
                    Just webSocketMessage ->
                        case ServerToClient.decode webSocketMessage of
                            Ok serverUpdate ->
                                handleServerUpdate serverUpdate tickedModel

                            Err _ ->
                                tickedModel

                    Nothing ->
                        tickedModel

              else
                tickedModel
            , Cmd.none
            )

        Input action ->
            ( model
            , ClientToServer.encode action
                |> sayToWebSocket model
            )

        ResizeWindow newSize ->
            ( { model | window = newSize }
            , Cmd.none
            )

        System input ->
            case input of
                SystemInput.KeyDown ToggleGraphics ->
                    ( { model | graphics = Graphics.cycle model.graphics }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        SelectFrom from ->
            ( { model | control = Just from }, Cmd.none )

        SelectTo to ->
            ( { model | control = Nothing }
            , case model.control of
                Nothing ->
                    Cmd.none

                Just (FromHand size) ->
                    FromClient.MoveNSizedPieceTo size to
                        |> FromClient.FromUser
                        |> ClientToServer.encode
                        |> sayToWebSocket model

                Just (FromBoard from) ->
                    FromClient.MoveFromTo from to
                        |> FromClient.FromUser
                        |> ClientToServer.encode
                        |> sayToWebSocket model
            )

        ConsiderLocation loc ->
            ( { model | consideredLocation = Just loc }
            , Cmd.none
            )

        ClearConsideration loc ->
            ( { model
                | consideredLocation =
                    if model.consideredLocation == Just loc then
                        Nothing

                    else
                        model.consideredLocation
              }
            , Cmd.none
            )

        CancelMove ->
            ( { model | control = Nothing }
            , Cmd.none
            )

        Process value ->
            case
                PortFunnels.processValue funnelDict value model.portFunnelState model
            of
                Err error ->
                    ( model, Cmd.none )

                Ok res ->
                    res

        StartGame ->
            ( { model
                | connectionStatus = Loading
                , gameStarted = False
                , control = Nothing
                , consideredLocation = Nothing
              }
            , case model.connectionStatus of
                Unused ->
                    openWebSocket model

                Loading ->
                    closeWebSocket model

                Open ->
                    closeWebSocket model

                Closed ->
                    openWebSocket model
            )


view : Model -> Document Msg
view { playerId, entities, room, graphics, window, metrics, topologyState, board, control, consideredLocation, connectionStatus, gameCategory } =
    let
        gameView =
            case gameCategory of
                BoardGame ->
                    View.NestingGame.view
                        playerId
                        board
                        control
                        consideredLocation
                        window
                        connectionStatus

                OtherGame ->
                    case graphics of
                        Graphics.Default ->
                            View.Default.view entities playerId room window

                        Graphics.PhysicsWireframe ->
                            View.PhysicsWireframe.view entities playerId room window

                        Graphics.OrvisTopology ->
                            case topologyState of
                                Nothing ->
                                    div [] [ p [] [ text "No Orvis metrics yet." ] ]

                                Just state ->
                                    View.OrvisTopology.view metrics state window
    in
    { title = "XYZ Engine"
    , body = List.singleton gameView
    }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ PortFunnels.subscriptions Process model
        , Browser.Events.onResize
            (\w h ->
                ResizeWindow
                    { width = toFloat w, height = toFloat h }
            )
        , Browser.Events.onKeyDown keyDownToMsg
        , Browser.Events.onKeyUp keyUpToMsg
        , Browser.Events.onAnimationFrame (\_ -> Tick)
        ]


entityToPair : FromServer.Entity -> ( Int, Entity )
entityToPair entity =
    ( entity.entityIdentity, Entity.fromServerEntity entity )


openWebSocket : Model -> Cmd Msg
openWebSocket model =
    WebSocket.makeOpen model.websocketLocation
        |> sendSomethingToWebSocket model


closeWebSocket : Model -> Cmd Msg
closeWebSocket model =
    WebSocket.makeClose model.websocketLocation
        |> sendSomethingToWebSocket model


sayToWebSocket : Model -> String -> Cmd Msg
sayToWebSocket model messageContent =
    WebSocket.makeSend model.websocketLocation messageContent
        |> sendSomethingToWebSocket model


sendSomethingToWebSocket : Model -> WebSocket.Message -> Cmd Msg
sendSomethingToWebSocket model =
    getCmdPort WebSocket.moduleName model
        |> WebSocket.send


handleServerUpdate : FromServer.Update -> Model -> Model
handleServerUpdate serverUpdate model =
    case serverUpdate of
        FromServer.BeginGame playerIdentity ->
            { model | playerId = Just playerIdentity, gameStarted = True }

        FromServer.GameState { room, entities } ->
            { model
                | entities =
                    entities
                        |> List.map entityToPair
                        |> Dict.fromList
                , room = room
            }

        FromServer.BoardState board ->
            { model | board = Just board }

        FromServer.Metrics rawMetrics ->
            let
                metrics =
                    Metrics.fromServerMetrics rawMetrics
            in
            { model
                | metrics = metrics
                , topologyState =
                    case model.topologyState of
                        Nothing ->
                            Just <|
                                View.OrvisTopology.construct metrics
                                    model.window

                        Just state ->
                            Just state
            }


keyDownToMsg : Decoder Msg
keyDownToMsg =
    Json.Decode.oneOf
        [ Json.Decode.map System SystemInput.keyDownDecoder
        , Json.Decode.map Input ClientToServer.keyDownDecoder
        ]


keyUpToMsg : Decoder Msg
keyUpToMsg =
    Json.Decode.oneOf
        [ Json.Decode.map System SystemInput.keyUpDecoder
        , Json.Decode.map Input ClientToServer.keyUpDecoder
        ]


init : flags -> Url -> key -> ( Model, Cmd Msg )
init flags { host, port_ } key =
    let
        portString =
            Maybe.map String.fromInt port_
                |> Maybe.withDefault ""

        websocketLocation =
            String.concat [ "ws://", host, ":", portString ]

        websocketState =
            WebSocket.initialState |> WebSocket.setAutoReopen websocketLocation False

        portFunnelState =
            { websocket = websocketState
            }

        model : Model
        model =
            { playerId = Nothing
            , entities = Dict.empty
            , room = { width = 0, height = 0 }
            , board = Nothing
            , window = { width = 250, height = 250 }
            , graphics = Graphics.Default
            , websocketLocation = websocketLocation
            , pendingWebSocketMessage = Nothing
            , gameStarted = False
            , metrics = Metrics.empty
            , topologyState = Nothing
            , control = Nothing
            , consideredLocation = Nothing
            , portFunnelState = portFunnelState
            , connectionStatus = Unused
            , gameCategory = BoardGame
            }
    in
    ( model
    , Cmd.batch
        [ Browser.Dom.getViewport
            |> Task.perform (\{ scene } -> ResizeWindow scene)
        ]
    )



-- PortFunnels boilerplate


handlers : List (Handler Model Msg)
handlers =
    [ WebSocketHandler socketHandler
    ]


funnelDict : FunnelDict Model Msg
funnelDict =
    PortFunnels.makeFunnelDict handlers getCmdPort


getCmdPort : String -> Model -> (Value -> Cmd Msg)
getCmdPort moduleName _ =
    PortFunnels.getCmdPort Process moduleName False


cmdPort : Value -> Cmd Msg
cmdPort =
    PortFunnels.getCmdPort Process "" False


socketHandler : Response -> PortFunnels.State -> Model -> ( Model, Cmd Msg )
socketHandler response state mdl =
    let
        model =
            { mdl | portFunnelState = state }
    in
    case response of
        WebSocket.ConnectedResponse r ->
            ( { model | connectionStatus = Open }
            , FromClient.JoinGame
                |> ClientToServer.encode
                |> sayToWebSocket model
            )

        WebSocket.MessageReceivedResponse { message } ->
            ( if model.gameStarted then
                { model | pendingWebSocketMessage = Just message }

              else
                case ServerToClient.decode message of
                    Ok serverUpdate ->
                        handleServerUpdate serverUpdate model

                    Err _ ->
                        model
            , Cmd.none
            )

        WebSocket.ClosedResponse { code, wasClean, expected } ->
            ( { model
                | connectionStatus =
                    Closed
              }
            , case model.connectionStatus of
                Loading ->
                    openWebSocket model

                _ ->
                    Cmd.none
            )

        _ ->
            ( model, Cmd.none )


closedString : WebSocket.ClosedCode -> Bool -> Bool -> String
closedString code wasClean expected =
    "code: "
        ++ WebSocket.closedCodeToString code
        ++ ", "
        ++ (if wasClean then
                "clean"

            else
                "not clean"
           )
        ++ ", "
        ++ (if expected then
                "expected"

            else
                "NOT expected"
           )
