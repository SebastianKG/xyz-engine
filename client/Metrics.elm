module Metrics exposing
    ( MailboxMetrics
    , Metrics
    , PortMetrics
    , RobotMetrics
    , SomeMetric(..)
    , empty
    , fromServerMetrics
    , name
    )

import Communication.WebSocket.Compiled.ServerToClient as Compiled
import Dict exposing (Dict)


type alias Metrics =
    { mailboxes : Dict Int MailboxMetrics
    , ports : Dict Int PortMetrics
    , robots : Dict Int RobotMetrics
    }


type SomeMetric
    = Mailbox MailboxMetrics
    | Port PortMetrics
    | Robot RobotMetrics


name : SomeMetric -> String
name metric =
    case metric of
        Mailbox { mailboxName } ->
            mailboxName

        Port { portName } ->
            portName

        Robot { robotName } ->
            robotName


type alias MailboxMetrics =
    { mailboxName : String
    }


type alias PortMetrics =
    { portName : String
    , subscriptionPoints : List Int
    , broadcastPoints : List Int
    }


type alias RobotMetrics =
    { robotName : String
    , readsFrom : List Int
    , writesTo : List Int
    , rendersTo : List Int
    }


empty : Metrics
empty =
    { mailboxes = Dict.empty
    , ports = Dict.empty
    , robots = Dict.empty
    }


fromServerMetrics : Compiled.MetricsUpdate -> Metrics
fromServerMetrics serverMetrics =
    let
        mailboxes : Dict Int MailboxMetrics
        mailboxes =
            List.foldr
                (\metric dict ->
                    Dict.insert
                        metric.mailboxId
                        { mailboxName = metric.mailboxName
                        }
                        dict
                )
                Dict.empty
                serverMetrics.mailboxes

        robots : Dict Int RobotMetrics
        robots =
            List.foldr
                (\metric dict ->
                    Dict.insert
                        metric.robotId
                        { robotName = metric.robotName
                        , readsFrom = metric.readsFrom
                        , writesTo = metric.writesTo
                        , rendersTo = metric.rendersTo
                        }
                        dict
                )
                Dict.empty
                serverMetrics.robots

        ports : Dict Int PortMetrics
        ports =
            List.foldr
                (\metric dict ->
                    Dict.insert
                        metric.portId
                        { portName = metric.portName
                        , subscriptionPoints = metric.subscriptionPoints
                        , broadcastPoints = metric.broadcastPoints
                        }
                        dict
                )
                Dict.empty
                serverMetrics.ports
    in
    { mailboxes = mailboxes
    , ports = ports
    , robots = robots
    }
