module Graphics exposing (Mode(..), cycle)


type Mode
    = Default
    | PhysicsWireframe
    | OrvisTopology


cycle : Mode -> Mode
cycle mode =
    case mode of
        Default ->
            PhysicsWireframe

        PhysicsWireframe ->
            OrvisTopology

        OrvisTopology ->
            Default
