module Communication.WebSocket.ClientToServer exposing
    ( encode
    , keyDownDecoder
    , keyUpDecoder
    )

import Communication.WebSocket.Compiled.ClientToServer as Compiled exposing (Key(..), Update(..), UserInput(..))
import Json.Decode as Decode exposing (Decoder)
import Json.Encode


encode : Update -> String
encode update =
    update
        |> Compiled.jsonEncUpdate
        |> Json.Encode.encode 0


keyDownDecoder : Decoder Update
keyDownDecoder =
    Decode.field "key" Decode.string
        |> Decode.andThen keyDecoder
        |> Decode.map (FromUser << KeyDown)


keyUpDecoder : Decoder Update
keyUpDecoder =
    Decode.field "key" Decode.string
        |> Decode.andThen keyDecoder
        |> Decode.map (FromUser << KeyUp)


keyDecoder : String -> Decoder Key
keyDecoder keyString =
    case String.map Char.toLower keyString of
        "arrowleft" ->
            Decode.succeed LArrow

        "arrowup" ->
            Decode.succeed UArrow

        "arrowright" ->
            Decode.succeed RArrow

        "arrowdown" ->
            Decode.succeed DArrow

        "z" ->
            Decode.succeed A

        "x" ->
            Decode.succeed B

        _ ->
            Decode.fail "Not a valid game key."
