module Communication.WebSocket.ServerToClient exposing (decode)

import Communication.WebSocket.Compiled.ServerToClient as Compiled
import Json.Decode


decode : String -> Result Json.Decode.Error Compiled.Update
decode =
    Json.Decode.decodeString Compiled.jsonDecUpdate
