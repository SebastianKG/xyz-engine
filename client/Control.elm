module Control exposing (BoardLocation, GameLocation(..))


type alias BoardLocation =
    ( Int, Int )


type GameLocation
    = FromHand Int
    | FromBoard BoardLocation
