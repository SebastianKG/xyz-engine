module SystemInput exposing
    ( Key(..)
    , SystemInput(..)
    , keyDownDecoder
    , keyUpDecoder
    , keyValue
    )

import Json.Decode as Decode exposing (Decoder)


type SystemInput
    = KeyDown Key
    | KeyUp Key


type Key
    = ToggleGraphics


keyDownDecoder : Decoder SystemInput
keyDownDecoder =
    Decode.field "key" Decode.string
        |> Decode.andThen keyDecoder
        |> Decode.map KeyDown


keyUpDecoder : Decoder SystemInput
keyUpDecoder =
    Decode.field "key" Decode.string
        |> Decode.andThen keyDecoder
        |> Decode.map KeyUp


keyDecoder : String -> Decoder Key
keyDecoder keyString =
    case String.map Char.toLower keyString of
        "q" ->
            Decode.succeed ToggleGraphics

        _ ->
            Decode.fail "Not a valid system input key"


keyValue : SystemInput -> Key
keyValue input =
    case input of
        KeyDown key ->
            key

        KeyUp key ->
            key
