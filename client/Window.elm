module Window exposing (Size)


type alias Size =
    { width : Float
    , height : Float
    }
