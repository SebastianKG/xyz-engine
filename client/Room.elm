module Room exposing (Room)


type alias Room =
    { width : Float, height : Float }
