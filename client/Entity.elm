module Entity exposing (Entity, fromServerEntity)

import Communication.WebSocket.Compiled.ServerToClient as Compiled


type alias Entity =
    { physics : Compiled.Physics
    , metrics : List ( String, Float )
    , info : List ( String, String )
    , switches : List ( String, Bool )
    }


fromServerEntity : Compiled.Entity -> Entity
fromServerEntity entity =
    { physics = entity.physics
    , metrics = entity.metrics
    , info = entity.info
    , switches = entity.switches
    }
