module Msg exposing (Msg(..))

import Communication.WebSocket.Compiled.ClientToServer as ClientToServer
import Control exposing (BoardLocation, GameLocation(..))
import Json.Encode exposing (Value)
import SystemInput exposing (SystemInput)
import Window


type Msg
    = NoOp
    | Tick
    | ResizeWindow Window.Size
    | Input ClientToServer.Update
    | System SystemInput
    | SelectFrom GameLocation
    | SelectTo BoardLocation
    | ConsiderLocation GameLocation
    | ClearConsideration GameLocation
    | CancelMove
    | Process Value
    | StartGame
