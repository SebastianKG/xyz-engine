module ConnectionStatus exposing (ConnectionStatus(..))


type ConnectionStatus
    = Unused
    | Loading
    | Open
    | Closed
