module Util exposing (bool, find, maybe, queryFor)


maybe : b -> (a -> b) -> Maybe a -> b
maybe default f =
    Maybe.withDefault default << Maybe.map f


bool : a -> a -> Bool -> a
bool onFalse onTrue booleanValue =
    if booleanValue then
        onTrue

    else
        onFalse


queryFor : k -> List ( k, v ) -> Maybe v
queryFor key tuples =
    tuples
        |> find (\( k, _ ) -> k == key)
        |> Maybe.map Tuple.second


find : (a -> Bool) -> List a -> Maybe a
find pred list =
    case list of
        [] ->
            Nothing

        x :: xs ->
            if pred x then
                Just x

            else
                find pred xs
