module View.OrvisTopology exposing (TopologyState, construct, tick, view)

import Dict
import Force exposing (State)
import Graph exposing (Edge, Graph, Node, NodeContext)
import Metrics exposing (Metrics)
import Msg exposing (Msg)
import Svg exposing (Svg, circle, g, line, svg, text, title)
import Svg.Attributes exposing (class, cx, cy, fill, r, stroke, strokeWidth, style, viewBox, x1, x2, y1, y2)
import View.Assets.Svg exposing (mailbox, port_, robot)
import Window


type alias TopologyState =
    { simulation : Force.State Int
    , lastSimulatedGraph : Graph Entity ()
    }


type alias Entity =
    Force.Entity Int { value : Maybe Metrics.SomeMetric }


view : Metrics -> TopologyState -> Window.Size -> Svg Msg
view metrics topology window =
    let
        graph =
            topology.lastSimulatedGraph

        linkElement : Graph Entity () -> Edge () -> Svg msg
        linkElement existingGraph edge =
            let
                source =
                    Maybe.withDefault
                        (Force.entity 0 Nothing)
                    <|
                        Maybe.map (.node >> .label) <|
                            Graph.get edge.from existingGraph

                target =
                    Maybe.withDefault
                        (Force.entity 0 Nothing)
                    <|
                        Maybe.map (.node >> .label) <|
                            Graph.get edge.to existingGraph
            in
            g []
                [ line
                    [ strokeWidth "1"
                    , stroke "rgb(0, 0, 0)"
                    , x1 <| String.fromFloat source.x
                    , y1 <| String.fromFloat source.y
                    , x2 <| String.fromFloat target.x
                    , y2 <| String.fromFloat target.y
                    ]
                    []
                ]

        nodeElement : Node Entity -> Svg msg
        nodeElement node =
            let
                metric =
                    node.label.value
            in
            g []
                [ title [] [ text <| Maybe.withDefault "" <| Maybe.map Metrics.name metric ]
                , case metric of
                    Nothing ->
                        g [] []

                    Just (Metrics.Mailbox _) ->
                        mailbox node.label.x node.label.y

                    Just (Metrics.Port _) ->
                        port_ node.label.x node.label.y

                    Just (Metrics.Robot _) ->
                        robot node.label.x node.label.y
                ]
    in
    svg
        [ viewBox <| "0 0 " ++ String.fromFloat window.width ++ " " ++ String.fromFloat window.height
        , style "border:1px;"
        ]
        [ Graph.edges graph
            |> List.map (linkElement graph)
            |> g []
        , Graph.nodes graph
            |> List.map nodeElement
            |> g []
        ]


construct : Metrics -> Window.Size -> TopologyState
construct metrics window =
    constructWithPreviousGraph metrics window Graph.empty


constructWithPreviousGraph :
    Metrics
    -> Window.Size
    -> Graph Entity ()
    -> TopologyState
constructWithPreviousGraph metrics window previousGraph =
    let
        graph =
            metricsToNodesAndEdges metrics
                |> (\( nodes, edges ) ->
                        Graph.fromNodesAndEdges
                            (List.map (addPreviousContext previousGraph) nodes)
                            edges
                   )

        link { from, to } =
            ( from, to )

        forces =
            [ Force.links <| List.map link <| Graph.edges graph
            , Force.manyBody <|
                List.map .id <|
                    Graph.nodes graph
            , Force.center (window.width / 2) (window.height / 2)
            ]
    in
    { simulation = Force.iterations 300 <| Force.simulation forces
    , lastSimulatedGraph = Graph.empty
    }


addPreviousContext : Graph Entity x -> Node Entity -> Node Entity
addPreviousContext previousGraph node =
    { node
        | label =
            previousGraph
                |> Graph.get node.id
                |> Maybe.map (.node >> .label)
                |> Maybe.withDefault node.label
    }


metricsToNodesAndEdges : Metrics -> ( List (Node Entity), List (Edge ()) )
metricsToNodesAndEdges metrics =
    List.map (\( id, mailbox ) -> ( mailboxToNode id mailbox, [] )) (Dict.toList metrics.mailboxes)
        ++ List.map (\( a, b ) -> portToNodesAndEdges a b) (Dict.toList metrics.ports)
        ++ List.map (\( a, b ) -> robotToNodesAndEdges a b) (Dict.toList metrics.robots)
        |> List.unzip
        |> Tuple.mapSecond List.concat


tick : Metrics -> Window.Size -> TopologyState -> TopologyState
tick metrics window ({ simulation, lastSimulatedGraph } as state) =
    let
        ( nodes, edges ) =
            metricsToNodesAndEdges metrics

        updatedNodes =
            List.map (addPreviousContext lastSimulatedGraph) nodes

        ( newState, list ) =
            Force.tick simulation <| List.map .label updatedNodes

        isNewNode node =
            node
                |> .id
                |> (\id -> not (Graph.member id lastSimulatedGraph))
    in
    if List.any isNewNode nodes && not (Graph.isEmpty lastSimulatedGraph) then
        constructWithPreviousGraph metrics window lastSimulatedGraph

    else
        { simulation = newState
        , lastSimulatedGraph = updateGraphWithList (Graph.fromNodesAndEdges nodes edges) list
        }


mailboxToNode : Int -> Metrics.MailboxMetrics -> Node Entity
mailboxToNode id mailboxMetrics =
    { id = id
    , label = Force.entity id <| Just <| Metrics.Mailbox mailboxMetrics
    }


portToNodesAndEdges : Int -> Metrics.PortMetrics -> ( Node Entity, List (Edge ()) )
portToNodesAndEdges id portMetrics =
    let
        portNode =
            { id = id
            , label = Force.entity id <| Just <| Metrics.Port portMetrics
            }

        portEdges =
            List.map (incomingEdge id) portMetrics.subscriptionPoints
                ++ List.map (outgoingEdge id) portMetrics.broadcastPoints
    in
    ( portNode, portEdges )


robotToNodesAndEdges : Int -> Metrics.RobotMetrics -> ( Node Entity, List (Edge ()) )
robotToNodesAndEdges id robotMetrics =
    let
        robotNode =
            { id = id
            , label = Force.entity id <| Just <| Metrics.Robot robotMetrics
            }

        robotEdges =
            List.map (incomingEdge id) robotMetrics.readsFrom
                ++ List.map (outgoingEdge id) robotMetrics.writesTo
                ++ List.map (outgoingEdge id) robotMetrics.rendersTo
    in
    ( robotNode, robotEdges )


outgoingEdge : Int -> Int -> Edge ()
outgoingEdge myId outgoingConnection =
    Graph.Edge myId outgoingConnection ()


incomingEdge : Int -> Int -> Edge ()
incomingEdge myId incomingConnection =
    outgoingEdge incomingConnection myId


updateContextWithValue : NodeContext Entity () -> Entity -> NodeContext Entity ()
updateContextWithValue nodeCtx value =
    let
        node =
            nodeCtx.node
    in
    { nodeCtx | node = { node | label = value } }


updateGraphWithList : Graph Entity () -> List Entity -> Graph Entity ()
updateGraphWithList =
    let
        graphUpdater value =
            Maybe.map (\ctx -> updateContextWithValue ctx value)
    in
    List.foldr (\node graph -> Graph.update node.id (graphUpdater node) graph)
