module View.Assets.Svg exposing (mailbox, port_, robot)

import Svg exposing (Svg, circle, rect, svg)
import Svg.Attributes exposing (cx, cy, d, fill, height, r, stroke, strokeWidth, viewBox, width)


robot : Float -> Float -> Svg msg
robot x y =
    svg
        [ width "24"
        , height "24"
        , viewBox "0 0 24 24"
        , Svg.Attributes.x <| String.fromFloat (x - 12)
        , Svg.Attributes.y <| String.fromFloat (y - 12)
        ]
        [ circle [ cx "12", cy "12", r "12", fill "rgb(200, 255, 200)" ] []
        , Svg.path
            [ d "M9 11.75c-.69 0-1.25.56-1.25 1.25s.56 1.25 1.25 1.25 1.25-.56 1.25-1.25-.56-1.25-1.25-1.25zm6 0c-.69 0-1.25.56-1.25 1.25s.56 1.25 1.25 1.25 1.25-.56 1.25-1.25-.56-1.25-1.25-1.25zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8 0-.29.02-.58.05-.86 2.36-1.05 4.23-2.98 5.21-5.37C11.07 8.33 14.05 10 17.42 10c.78 0 1.53-.09 2.25-.26.21.71.33 1.47.33 2.26 0 4.41-3.59 8-8 8z" ]
            []
        ]


port_ : Float -> Float -> Svg msg
port_ x y =
    svg
        [ width "24"
        , height "24"
        , viewBox "0 0 24 24"
        , Svg.Attributes.x <| String.fromFloat (x - 12)
        , Svg.Attributes.y <| String.fromFloat (y - 12)
        ]
        [ circle [ cx "12", cy "12", r "12", fill "rgb(200, 200, 255)" ] []
        , Svg.path
            [ d "M16.01 7L16 3h-2v4h-4V3H8v4h-.01C7 6.99 6 7.99 6 8.99v5.49L9.5 18v3h5v-3l3.5-3.51v-5.5c0-1-1-2-1.99-1.99z" ]
            []
        ]


mailbox : Float -> Float -> Svg msg
mailbox x y =
    svg
        [ width "24"
        , height "24"
        , viewBox "0 0 24 24"
        , Svg.Attributes.x <| String.fromFloat (x - 12)
        , Svg.Attributes.y <| String.fromFloat (y - 12)
        ]
        [ rect [ width "24", height "24", fill "rgb(255, 255, 200)" ] []
        , Svg.path
            [ d "M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H4V8l8 5 8-5v10zm-8-7L4 6h16l-8 5z" ]
            []
        ]
