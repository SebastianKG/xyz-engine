module View.Default exposing (view)

import Collage exposing (Collage)
import Collage.Layout as Collage
import Collage.Render as Collage
import Collage.Text
import Color
import Dict exposing (Dict)
import Entity exposing (Entity)
import Html exposing (Html, div)
import Msg exposing (Msg)
import Room exposing (Room)
import Util exposing (bool, queryFor)
import Window


view : Dict Int Entity -> Maybe Int -> Room -> Window.Size -> Html Msg
view entities playerId room window =
    let
        otherShipColor =
            Color.blue

        playerShipColor =
            Color.lightBlue

        viewEntity : ( Int, Entity ) -> Collage msg
        viewEntity ( id, { info, physics } ) =
            let
                { kinematics } =
                    physics

                { position, rotation } =
                    kinematics

                isPlayer =
                    case playerId of
                        Nothing ->
                            False

                        Just pid ->
                            pid == id

                entityCollage : Collage msg
                entityCollage =
                    case queryFor "entityType" info of
                        Just "spaceship" ->
                            Collage.polygon
                                [ ( -20, -5 )
                                , ( 0, 15 )
                                , ( 20, -5 )
                                ]
                                |> Collage.filled
                                    (Collage.uniform
                                        (if isPlayer then
                                            playerShipColor

                                         else
                                            otherShipColor
                                        )
                                    )
                                |> Collage.rotate (degrees <| rotation + 270)

                        Just "bullet" ->
                            Collage.ngon 8 8
                                |> Collage.filled
                                    (Collage.uniform
                                        Color.lightGreen
                                    )
                                |> Collage.rotate
                                    (degrees <| toFloat <| modBy 360 (id * 45))

                        _ ->
                            Collage.Text.fromString "?"
                                |> Collage.rendered
            in
            entityCollage
                |> Collage.shift position

        roomCollage : Collage msg
        roomCollage =
            let
                outerLine =
                    Collage.rectangle room.width room.height
                        |> Collage.outlined
                            (Collage.dot
                                Collage.thin
                                (Collage.uniform <|
                                    Color.black
                                )
                            )

                background =
                    Collage.rectangle (room.width + 32) (room.height + 32)
                        |> Collage.filled
                            (Collage.uniform
                                Color.lightGray
                            )
            in
            Collage.group
                (if room.width > 0 then
                    [ outerLine, background ]

                 else
                    []
                )
    in
    List.map viewEntity (Dict.toList entities)
        ++ List.singleton roomCollage
        |> Collage.stack
        |> Collage.svgBox ( window.width, window.height )
        |> List.singleton
        |> div []
