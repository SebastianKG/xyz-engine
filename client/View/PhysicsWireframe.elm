module View.PhysicsWireframe exposing (view)

import Collage exposing (Collage, defaultLineStyle)
import Collage.Layout as Collage
import Collage.Render as Collage
import Color
import Dict exposing (Dict)
import Entity exposing (Entity)
import Html exposing (Html, div)
import Msg exposing (Msg)
import Room exposing (Room)
import Window


view : Dict Int Entity -> Maybe Int -> Room -> Window.Size -> Html Msg
view entities playerId room window =
    let
        viewEntity : ( Int, Entity ) -> Collage msg
        viewEntity ( id, { metrics, physics, switches } ) =
            let
                entityColor =
                    Color.white

                { bounds, kinematics } =
                    physics

                outlined collage =
                    collage
                        |> Collage.outlined
                            { defaultLineStyle | fill = Collage.uniform entityColor }

                entityCollage : Collage msg
                entityCollage =
                    let
                        radius =
                            bounds

                        circularForm =
                            Collage.circle radius
                                |> outlined

                        rotationIndicatorForm =
                            Collage.rectangle radius 1
                                |> outlined
                                |> Collage.shiftX (radius / 2)
                    in
                    Collage.group
                        [ circularForm
                        , rotationIndicatorForm
                        ]

                { position, rotation } =
                    kinematics
            in
            entityCollage
                |> Collage.rotate (degrees rotation)
                |> Collage.shift position

        roomCollage : Collage msg
        roomCollage =
            let
                outerLine =
                    Collage.rectangle room.width room.height
                        |> Collage.outlined
                            (Collage.dot Collage.thin
                                (Collage.uniform Color.white)
                            )

                background =
                    Collage.rectangle
                        window.width
                        window.height
                        |> Collage.filled (Collage.uniform Color.black)
            in
            Collage.group [ outerLine, background ]
    in
    List.map viewEntity (Dict.toList entities)
        ++ List.singleton roomCollage
        |> Collage.stack
        |> Collage.svgBox ( window.width, window.height )
        |> List.singleton
        |> div []
