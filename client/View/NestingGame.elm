module View.NestingGame exposing (view)

import Communication.WebSocket.Compiled.ServerToClient as FromServer
import ConnectionStatus exposing (ConnectionStatus(..))
import Control exposing (GameLocation(..))
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (Html)
import Msg exposing (Msg(..))
import Window


white : Element.Color
white =
    Element.rgb255 255 255 255


black : Element.Color
black =
    Element.rgb255 0 0 0


gray : Element.Color
gray =
    Element.rgb255 200 200 200


tilePrimaryColor : Element.Color
tilePrimaryColor =
    Element.rgb255 150 150 150


tileAlternateColor : Element.Color
tileAlternateColor =
    Element.rgb255 100 100 100


highlightColor : Element.Color
highlightColor =
    Element.rgb255 255 200 50


positiveColor : Element.Color
positiveColor =
    Element.rgb255 100 255 100


negativeColor : Element.Color
negativeColor =
    Element.rgb255 255 100 100


siteBackground : Element.Color
siteBackground =
    Element.rgb255 255 255 255


tileHeightRatio =
    2 / 13


view :
    Maybe Int
    -> Maybe FromServer.GameModel
    -> Maybe GameLocation
    -> Maybe GameLocation
    -> Window.Size
    -> ConnectionStatus
    -> Html Msg
view playerId game selection consideredLocation { width, height } connectionStatus =
    let
        tileSize =
            tileHeightRatio
                * height
                |> round

        viewGame : Completion -> FromServer.GameBoard -> Maybe FromServer.WinningMethod -> Element Msg
        viewGame completion { blackId, whiteId, blackPieces, whitePieces, board, currentTurn } winningMethod =
            let
                ( ( topPlayer, topPieces ), ( bottomPlayer, bottomPieces ) ) =
                    if playerId == Just whiteId then
                        ( ( FromServer.Black, blackPieces ), ( FromServer.White, whitePieces ) )

                    else
                        ( ( FromServer.White, whitePieces ), ( FromServer.Black, blackPieces ) )

                itsMyTurn =
                    if playerId == Just whiteId then
                        currentTurn == FromServer.White

                    else
                        currentTurn == FromServer.Black

                justBegun =
                    board
                        |> List.map
                            (List.all
                                (\cell ->
                                    isNothing cell
                                )
                            )
                        |> List.all identity

                startGameHeadline =
                    headline Nothing (Font.size <| round <| toFloat tileSize / 5)

                endGameHeadline =
                    headline
                        (Just <|
                            Input.button
                                [ Background.color white
                                , Element.padding <| round <| toFloat tileSize / 10
                                , Border.color black
                                , Border.width 1
                                ]
                                { onPress = Just StartGame, label = Element.text "Play Again" }
                        )
                        (Font.size <| round <| toFloat tileSize / 5)

                headline subText fontSize headlineText background =
                    let
                        headlineTextElement =
                            Element.text headlineText
                                |> Element.el [ Element.centerX, Element.centerY, fontSize ]

                        elements =
                            case subText of
                                Nothing ->
                                    [ headlineTextElement ]

                                Just subtitle ->
                                    [ headlineTextElement
                                    , subtitle |> Element.el [ Element.centerX, Element.centerY ]
                                    ]
                    in
                    elements
                        |> Element.column
                            [ Element.centerX
                            , Element.centerY
                            , Element.spacing <| round <| toFloat tileSize / 15
                            ]
                        |> Element.el
                            [ Element.width <| Element.px <| tileSize * 4
                            , Element.height <| Element.px <| round <| toFloat tileSize * 16 / 15
                            , Background.color background
                            ]
            in
            Element.column [ Element.centerX, Element.centerY ] <|
                [ case completion of
                    YouWon ->
                        endGameHeadline "You win!" positiveColor

                    YouLost ->
                        endGameHeadline "You lose." negativeColor

                    StillInProgress ->
                        if justBegun then
                            if itsMyTurn then
                                startGameHeadline "You move first." positiveColor

                            else
                                startGameHeadline "They move first." highlightColor

                        else
                            playerPieces topPieces topPlayer (not itsMyTurn) False
                , viewBoard itsMyTurn completion winningMethod bottomPlayer board
                , playerPieces bottomPieces bottomPlayer itsMyTurn True
                ]

        viewBoard : Bool -> Completion -> Maybe FromServer.WinningMethod -> FromServer.PieceColor -> List (List (Maybe FromServer.Piece)) -> Element Msg
        viewBoard itsMyTurn completion winningMethod playerColor rows =
            let
                toTileView : Int -> Int -> Maybe FromServer.Piece -> TileView Msg
                toTileView x y cell =
                    let
                        belongsToOtherPlayer piece =
                            case piece of
                                Nothing ->
                                    False

                                Just (FromServer.Piece color _) ->
                                    color /= playerColor

                        addConsiderationHighlight attrs =
                            if consideredLocation == Just (FromBoard ( x, y )) then
                                Background.color highlightColor :: attrs

                            else
                                attrs

                        winHighlights =
                            winningMethod
                                |> Maybe.map winHighlightedTiles
                                |> Maybe.withDefault []

                        addWinHighlight ( tileX, tileY ) attrs =
                            let
                                winColor color =
                                    if List.member ( tileX, tileY ) winHighlights then
                                        Background.color color :: attrs

                                    else
                                        attrs
                            in
                            case completion of
                                StillInProgress ->
                                    attrs

                                YouWon ->
                                    winColor positiveColor

                                YouLost ->
                                    winColor negativeColor
                    in
                    { attrs =
                        (if itsMyTurn then
                            case selection of
                                Nothing ->
                                    if isJust cell && not (belongsToOtherPlayer cell) then
                                        addConsiderationHighlight
                                            [ Events.onClick (SelectFrom (FromBoard ( x, y )))
                                            , Events.onMouseEnter (ConsiderLocation (FromBoard ( x, y )))
                                            , Events.onMouseLeave (ClearConsideration (FromBoard ( x, y )))
                                            , Element.pointer
                                            ]

                                    else
                                        []

                                Just (FromBoard coords) ->
                                    if coords == ( x, y ) then
                                        [ Background.color highlightColor
                                        , Events.onClick CancelMove
                                        , Element.pointer
                                        ]

                                    else
                                        [ Events.onClick (SelectTo ( x, y ))
                                        , Events.onMouseEnter (ConsiderLocation (FromBoard ( x, y )))
                                        , Events.onMouseLeave (ClearConsideration (FromBoard ( x, y )))
                                        , Element.pointer
                                        ]
                                            |> addConsiderationHighlight

                                Just _ ->
                                    [ Events.onClick (SelectTo ( x, y ))
                                    , Events.onMouseEnter (ConsiderLocation (FromBoard ( x, y )))
                                    , Events.onMouseLeave (ClearConsideration (FromBoard ( x, y )))
                                    , Element.pointer
                                    ]
                                        |> addConsiderationHighlight

                         else
                            []
                        )
                            |> addWinHighlight ( x, y )
                    , color =
                        if modBy 2 (x + y) == 0 then
                            tilePrimaryColor

                        else
                            tileAlternateColor
                    , pieceContext =
                        case cell of
                            Nothing ->
                                NoPiece

                            Just (FromServer.Piece color size) ->
                                WithSizeLabel
                                    (displayServerColor color)
                                    size
                    }
            in
            rows
                |> List.indexedMap
                    (\y row ->
                        List.indexedMap (\x cell -> viewTile tileSize <| toTileView x y cell) row
                            |> Element.column []
                    )
                |> Element.row []

        fromCoordinates : Maybe ( Int, Int )
        fromCoordinates =
            case selection of
                Just (FromBoard location) ->
                    Just location

                _ ->
                    Nothing

        playerPieces : FromServer.PieceCount -> FromServer.PieceColor -> Bool -> Bool -> Element Msg
        playerPieces { smallest, smaller, bigger, biggest } pieceColor itsMyTurn isPlayer =
            let
                toTileView : Int -> Int -> TileView Msg
                toTileView i count =
                    let
                        addConsiderationHighlight attrs =
                            if consideredLocation == Just (FromHand i) && count > 0 then
                                Background.color highlightColor :: attrs

                            else
                                attrs

                        addClickListener attrs =
                            if count > 0 then
                                [ Events.onClick (SelectFrom (FromHand i))
                                , Element.pointer
                                ]
                                    ++ attrs

                            else
                                attrs
                    in
                    { attrs =
                        if isPlayer && itsMyTurn then
                            case selection of
                                Just (FromHand size) ->
                                    if size == i then
                                        [ Background.color highlightColor
                                        , Events.onClick CancelMove
                                        ]

                                    else
                                        [ Events.onMouseEnter (ConsiderLocation (FromHand i))
                                        , Events.onMouseLeave (ClearConsideration (FromHand i))
                                        ]
                                            |> addConsiderationHighlight
                                            |> addClickListener

                                Just (FromBoard _) ->
                                    addConsiderationHighlight []

                                _ ->
                                    [ Events.onMouseEnter (ConsiderLocation (FromHand i))
                                    , Events.onMouseLeave (ClearConsideration (FromHand i))
                                    ]
                                        |> addConsiderationHighlight
                                        |> addClickListener

                        else
                            []
                    , color = gray
                    , pieceContext =
                        WithCount (displayServerColor pieceColor)
                            (case i of
                                0 ->
                                    FromServer.Smallest

                                1 ->
                                    FromServer.Smaller

                                2 ->
                                    FromServer.Bigger

                                3 ->
                                    FromServer.Biggest

                                _ ->
                                    -- silly default case
                                    FromServer.Biggest
                            )
                            count
                    }

                turnStripColor =
                    if itsMyTurn then
                        highlightColor

                    else
                        gray

                noBorders =
                    { left = 0, top = 0, right = 0, bottom = 0 }

                turnStripBorderWidths =
                    if isPlayer then
                        { noBorders | top = round <| toFloat tileSize / 15 }

                    else
                        { noBorders | bottom = round <| toFloat tileSize / 15 }
            in
            List.indexedMap (\i count -> viewTile tileSize <| toTileView i count)
                [ smallest
                , smaller
                , bigger
                , biggest
                ]
                |> (\pieceList ->
                        if not isPlayer then
                            List.reverse pieceList

                        else
                            pieceList
                   )
                |> Element.row [ Border.color turnStripColor, Border.widthEach turnStripBorderWidths ]

        homeScreen =
            [ Element.text "TicTacToe II"
                |> Element.el
                    [ Font.size <| round <| toFloat tileSize / 2
                    , Font.family [ Font.typeface "Times New Roman", Font.serif ]
                    , Element.centerX
                    ]
            , Element.text "The sequel to TicTacToe we've all been waiting for."
                |> Element.el
                    [ Element.centerX
                    , Font.size <| round <| toFloat tileSize / 6
                    ]
            , Input.button [ Element.centerX ] { onPress = Just StartGame, label = Element.text "Click to Play" }
                |> Element.el
                    [ Background.color positiveColor
                    , Element.padding <| round <| toFloat tileSize / 10
                    , Element.centerX
                    , Border.color black
                    , Border.width 1
                    ]
            ]
                |> Element.column [ Element.centerX, Element.centerY, Element.spacing <| round <| toFloat tileSize / 3 ]
    in
    Element.layout [ Font.family [ Font.typeface "Verdana", Font.sansSerif ], Background.color siteBackground ] <|
        case connectionStatus of
            Unused ->
                homeScreen

            Loading ->
                Element.text "Waiting for game connection..."
                    |> Element.el [ Element.centerX, Element.centerY ]

            Closed ->
                Element.text "Disconnected from game."
                    |> Element.el [ Element.centerX, Element.centerY ]

            Open ->
                case game of
                    Nothing ->
                        homeScreen

                    Just runningGame ->
                        case runningGame of
                            FromServer.Loading ->
                                Element.text "Waiting for an opponent..."
                                    |> Element.el [ Element.centerX, Element.centerY ]

                            FromServer.InProgress gameState ->
                                viewGame StillInProgress gameState Nothing

                            FromServer.WonBy winnerId gameState winningMethod ->
                                Just winningMethod
                                    |> viewGame
                                        (if playerId == Just winnerId then
                                            YouWon

                                         else
                                            YouLost
                                        )
                                        gameState


type Completion
    = YouWon
    | YouLost
    | StillInProgress


colorToString : FromServer.PieceColor -> String
colorToString color =
    case color of
        FromServer.White ->
            "White"

        FromServer.Black ->
            "Black"


displayServerColor : FromServer.PieceColor -> Element.Color
displayServerColor color =
    case color of
        FromServer.White ->
            white

        FromServer.Black ->
            black


type alias TileView msg =
    { attrs : List (Element.Attribute msg)
    , color : Element.Color
    , pieceContext : PieceContext
    }


type PieceContext
    = NoPiece
    | WithSizeLabel Element.Color FromServer.PieceSize
    | WithCount Element.Color FromServer.PieceSize Int


viewTile : Int -> TileView Msg -> Element Msg
viewTile tileSize tile =
    let
        wrapper =
            Element.el
                ([ Background.color tile.color
                 , Element.width <| Element.px tileSize
                 , Element.height <| Element.px tileSize
                 ]
                    ++ tile.attrs
                )

        sizeInfo : FromServer.PieceSize -> ( String, Float )
        sizeInfo pieceSize =
            case pieceSize of
                FromServer.Smallest ->
                    ( "S", 0.45 )

                FromServer.Smaller ->
                    ( "M", 0.6 )

                FromServer.Bigger ->
                    ( "L", 0.75 )

                FromServer.Biggest ->
                    ( "XL", 0.9 )

        actualElements pieceLabel pieceColor size =
            let
                ( label, sizeMultiplier ) =
                    sizeInfo size

                pieceSize =
                    toFloat tileSize
                        * sizeMultiplier
                        |> round
            in
            Element.text pieceLabel
                |> Element.el
                    [ Element.centerX
                    , Element.centerY
                    , Font.size <| floor <| toFloat tileSize * (2 / 3) * sizeMultiplier
                    , Font.color gray
                    ]
                |> Element.el
                    [ pieceSize
                        |> Element.px
                        |> Element.width
                    , pieceSize
                        |> Element.px
                        |> Element.height
                    , Background.color pieceColor
                    , Element.centerX
                    , Element.centerY
                    , Border.rounded <| pieceSize * 10
                    ]
                |> wrapper
    in
    case tile.pieceContext of
        NoPiece ->
            Element.none
                |> wrapper

        WithSizeLabel color size ->
            let
                ( label, _ ) =
                    sizeInfo size
            in
            actualElements label color size

        WithCount color size count ->
            actualElements (String.fromInt count) color size


isJust : Maybe a -> Bool
isJust =
    not << isNothing


isNothing : Maybe a -> Bool
isNothing v =
    case v of
        Nothing ->
            True

        _ ->
            False


winHighlightedTiles : FromServer.WinningMethod -> List ( Int, Int )
winHighlightedTiles winningMethod =
    case winningMethod of
        FromServer.OtherPlayerLeft ->
            []

        FromServer.DiagonalLine direction ->
            case direction of
                FromServer.TopLeftBottomRight ->
                    List.map2 Tuple.pair (List.range 0 3) (List.range 0 3)

                FromServer.BottomLeftTopRight ->
                    List.map2 Tuple.pair (List.range 0 3) (List.reverse <| List.range 0 3)

        FromServer.HorizontalLine y ->
            List.map (\x -> ( x, y )) (List.range 0 3)

        FromServer.VerticalLine x ->
            List.map (\y -> ( x, y )) (List.range 0 3)
