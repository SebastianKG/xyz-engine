{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE NamedFieldPuns #-}

import qualified External.Server as Server
import External.Server (GameRules(..))
import qualified Sample.NestingGame as NestingGame
import qualified Sample.SpaceChase as SpaceChase
import System.Console.CmdArgs.Implicit (Data, Typeable, cmdArgs)

newtype Args = Args
  { game :: Game
  } deriving (Data, Typeable)

data Game
  = SpaceChase
  | NestingGame
  deriving (Data, Typeable)

main :: IO ()
main = do
  Args {game} <- cmdArgs Args {game = SpaceChase}
  case game of
    SpaceChase ->
      Server.run
        GameRules
          { maxPlayerCount = 4
          , gamesPerServer = 1
          , framesPerSecond = 60
          , withMetrics = True
          , gameLogic = SpaceChase.logic
          , lockWhenFull = False
          }
    NestingGame ->
      Server.run
        GameRules
          { maxPlayerCount = 2
          , gamesPerServer = 500
          , framesPerSecond = 60
          , withMetrics = False
          , gameLogic = NestingGame.logic
          , lockWhenFull = True
          }
